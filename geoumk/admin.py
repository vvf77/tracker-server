from django.contrib import admin
from .models import *

class MobileUserAdmin(admin.ModelAdmin):
    list_display = ('last_name','first_name','middle_name', 'department')

class DepartmentAdmin(admin.ModelAdmin):
    model = Group
    list_display = ('name',)

class DirectionAdmin(admin.ModelAdmin):
    model = Direction
    list_display = ('name',)

class BranchAdmin(admin.ModelAdmin):
    model = Branch
    list_display = ('name',)

class MobileDeviceAdmin(admin.ModelAdmin):
    model = MobileDevice
    list_display = ('phone',)

admin.site.register(Direction, DirectionAdmin)
admin.site.register(Branch, BranchAdmin)
admin.site.register(Group, DepartmentAdmin)
admin.site.register(MobileUser, MobileUserAdmin)
admin.site.register(MobileDevice, MobileDeviceAdmin)

# admin.site.register(MobileUser, MobileUserAdmin)
# admin.site.register(MobileUser, MobileUserAdmin)
# admin.site.register(MobileUser, MobileUserAdmin)

