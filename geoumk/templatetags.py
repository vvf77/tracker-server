from django.template.loader_tags import register

__author__ = 'vvf'

@register.filter
def keyvalue(dict, key, default=''):
    return dict.get(key,default)