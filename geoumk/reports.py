import json
from django.template.defaulttags import register
from geoumk.utils import create_date_filter, get_address, hours2str, RecalcStops

__author__ = 'vvf'
'''
Отслеживание торговых агентов: Отчеты.
Общий отчет:
    Дата
    Направление - филиал - группа
    ИД ТП
    ФИО ТП
    Номер телефона ТП (?)
    Время в движении за день (сумма времен между остановками)
    Средняя скорость в движении (средняя скорость всех точек которые не входят в остановки)
    Пройденное растояние (ресурсоемкая операция: просмотреть все точки и сложить последовательно растояние между соседними точками)
    Среднее время остановки
    Максимальное время остановки
    Количество остановок
    Время начала движения (конец первой остановки или врмея первой точки).
    Время окончания движения (начало последней остановки/точки)
    Адрес последней остановки/точки (будет востребован для текущего дня)

Отчет по одному ТП, за один день:
    В заголовоке:
        Дата
        Направление - филиал - группа
        ИД ТП
        ФИО ТП
        Номер телефона ТП
        Ссылка на маршрут на карте
    Таблица
        Время (каждые 10-15-20-30 минут: 6,4,3 или 2 строки на час)
        Адрес
        Скорость
        Растояние от предыдущей строки
        Ссылка на точку остановки на карте если это точка остановки
    Итого
        Пройденное растояние
        Среднее время остановки
        Максимальное время остановки
        Количество остановок
'''
app_url_path = '/static/index.html'
reports_fields = dict(
    common=dict(
        title_tpl='Отчет по торговым представителям за {date}',
        table=[
            {'displayName': 'Дата',    'field': 'date'},
            {'displayName': 'Поразделение', 'field': 'department'},
            {'displayName': 'ИД ТП', 'field': 'tp_id'},
            {'displayName': 'ФИО ТП', 'field': 'tp_fio'},
            {'displayName': 'Номер телефона ТП', 'field': 'tp_phone', 'link_field': 'url'},
            {'displayName': 'Время в движении за день', 'field': 'moving_time'},
            {'displayName': 'Средняя скорость в движении', 'field': 'avg_speed'},
            {'displayName': 'Пройденное расстояние', 'field': 'distance'},
            {'displayName': 'Среднее время остановки', 'field': 'avg_stop_time'},
            {'displayName': 'Максимальное время остановки', 'field': 'max_stop_time'},
            {'displayName': 'Количество остановок', 'field': 'stops_count'},
            {'displayName': 'Время начала движения', 'field': 'start_time'},
            {'displayName': 'Время окончания движения', 'field': 'finish_time'},
            {'displayName': 'Адрес последней остановки', 'field': 'last_address', 'link_field': 'stop_url'}
        ]
    ),
    detail=dict(
        title_tpl='Отчет по {tp_fio} за {date}',
        header=[
            {'displayName': 'Дата',    'field': 'date'},
            {'displayName': 'Поразделение', 'field': 'department'},
            {'displayName': 'ИД ТП', 'field': 'tp_id'},
            {'displayName': 'ФИО ТП', 'field': 'tp_fio'},
            {'displayName': 'Номер телефона ТП', 'field': 'tp_phone'},
            {'displayName': 'Ссылка на маршрут на карте', 'link_field': 'url', 'field': 'url'},
        ],
        table=[
            {'displayName': '№ п/п', 'field': 'npp', 'width':'20px'},
            {'displayName': 'Время', 'field': 'time'},
            {'displayName': 'Адрес', 'field': 'address'},
            {'displayName': 'Пройденное расстояние', 'field': 'distance', 'width':'*'},
            {'displayName': 'Время', 'field': 'time_diff'},
            {'displayName': 'Скорость', 'field': 'speed'},
            {'displayName': 'Ссылка на точку остановки на карте если это точка остановки', 'field': 'url', 'link_field': 'url'}

        ],
        footer=[
            {'displayName': 'Пройденное растояние', 'field': 'distance'},
            {'displayName': 'Среднее время остановки', 'field': 'avg_stop_time'},
            {'displayName': 'Максимальное время остановки', 'field': 'stops_time'},
            {'displayName': 'Количество остановок', 'field': 'stops_count'},
            {'displayName': 'Количество точек', 'field': 'points_count' }
        ]
    )
)
from .models import *
from geoumk import utils
from django.http import HttpResponse
from django.shortcuts import render

@register.filter
def keyvalue(dict, key, default=''):
    return dict.get(key,default)


class Report:

    def __init__(self, get_data, report_type):
        #report_type = self.__class__.__name__.replace('Report','').lower()
        self.report_type = report_type
        self.info = reports_fields[report_type]
        self.get_data = get_data

    def __call__(self, request, response_type, **kwargs):
        result = dict(
            meta_data=  self.info.copy(),
            data=       self.get_data(request, kwargs)
        )
        result['meta_data']['title'] = result['meta_data']['title_tpl'].format(**result['data']['header'])
        del result['meta_data']['title_tpl']
        if response_type == 'json':
            return HttpResponse(json.dumps(result), content_type="application/json")
        elif response_type in ['csv','txt']:
            csv = [';'.join([field['displayName'] for field in self.info['table']])]
            csv += [';'.join([str(row[field['field']] or '-') for field in self.info['table']]) for row in result['data']['table']]
            resp = HttpResponse( '\n'.join(csv), content_type="text/"+response_type)
            resp['Content-Disposition']= 'attachment;filename={}.{}'.format(self.report_type,response_type)
            return resp
        else:
            result['isXls']= (response_type == 'xls')
            resp = render(request, 'report.html', result, content_type="application/vnd.ms-excel" if response_type == 'xls' else 'text/html')
            if response_type == 'xls':
                resp['Content-Disposition']= 'attachment;filename={}.xls'.format(self.report_type)
            return resp


def as_report(rep_type):
    def decor(get_data_fn):
        return Report(get_data_fn,rep_type)
    return decor

@as_report('common')
def common_report(request, params):

    timefilter = create_date_filter('when_gathered',
                                    for_date=request.REQUEST.get('fordate'),
                                    time_from=request.REQUEST.get('fromtime'),
                                    time_to=request.REQUEST.get('totime'))
    stopfilter = create_date_filter('time_stop',
                                    for_date=request.REQUEST.get('fordate'),
                                    time_from=request.REQUEST.get('fromtime'),
                                    time_to=request.REQUEST.get('totime'))

    date = datetime.strptime(stopfilter['time_stop__gt'], '%Y-%m-%d %H:%M:%S')
    table = []
    dev_ids = request.REQUEST.get('devices')
    if dev_ids:
        dev_ids = dev_ids.split(',')

        RecalcStops()(device_id=dev_ids, for_date=date.strftime('%Y-%m-%d'), force=request.REQUEST.get('recalc_stops') and True )

        filter = dict(id__in=dev_ids, active=True)
        devs = MobileDevice.objects.filter(**filter).order_by('user')
        for dev in devs:

            moving_points = dev.point_set.exclude(in_stop=None).filter(**timefilter).order_by('when_gathered')
            stops = dev.stoppoint_set.filter(**stopfilter).order_by('time_stop')
            moving_time = 0.0
            distance = 0.0
            last = None
            speed_sum = 0.0
            speed_count = 0
            for pnt in moving_points:
                if last:
                    moving_time = moving_time + pnt.when_gathered.timestamp()-last.when_gathered.timestamp()
                    distance += pnt.distanceTo(last)
                last = pnt
                if pnt.speed:
                    speed_count += 1
                    speed_sum += pnt.speed
            if speed_count > 0:
                speed = speed_sum/speed_count
            else:
                speed = '-'
            moving_time = moving_time/3600
            staying_time = 0
            max_stop_time = 0
            stops_count = stops.count()
            for stop in stops:
                staying_time += stop.how_long
                if max_stop_time < stop.how_long:
                    max_stop_time = stop.how_long

            last_stop = stops.last()
            last_point = last_stop and last_stop.point_stop
            if last and last_point and last.when_gathered > last_point.when_gathered:
                last_point = last
            first_stop = stops.first()
            distance=distance/1000
            table.append(dict(
                date=date.strftime('%d.%m.%Y'),
                department   =str(dev.department),  # there will be group and get group full name
                tp_id        =dev.user_id or '0',
                tp_fio       =str(dev.user),
                tp_phone     =dev.phone,
                moving_time  =moving_time and hours2str(moving_time), # Время в движении за день
                avg_speed    ='{:5.2f}'.format(distance / moving_time) if moving_time>0 and distance>0 else '-',
                svg_speed2   =speed,
                distance     =distance and  '{:5.2f} км'.format(distance),
                avg_stop_time=hours2str(staying_time/stops_count) if stops_count>0 else '-',
                staying_time =staying_time and hours2str(staying_time),
                stops_count  =stops_count,
                max_stop_time=max_stop_time and hours2str(max_stop_time),
                start_time   =first_stop and first_stop.time_continue.strftime('%d.%m.%Y %H:%M') or None,
                finish_time  =last_stop and last_stop.time_stop.strftime('%d.%m.%Y %H:%M') or None,
                last_address =last_point and get_address(last_point.lat, last_point.lng) or None,
                url = '{}#/Device/{}/{:%Y-%m-%d}'.format(app_url_path, dev.id, date),
                stop_url = last_stop and '{}#/Stop/{}/{:%Y-%m-%d}'.format(app_url_path, last_stop.id, date),
            ))

    return dict(
        header=dict(
            date=date.strftime('%d.%m.%Y')
        ),
        table=table
    )

@as_report('detail')
def detail_report(request, params):

    timefilter = create_date_filter('when_gathered',
                                    for_date=request.REQUEST.get('fordate'),
                                    time_from=request.REQUEST.get('fromtime'),
                                    time_to=request.REQUEST.get('totime'))

    date = datetime.strptime(timefilter['when_gathered__gt'], '%Y-%m-%d %H:%M:%S')
    table = []
    header={
        'date': date.strftime('%d.%m.%Y')
    }
    footer={
    }
    #for row in
    dev_id = params.get('dev_id') or request.REQUEST.get('device')
    if dev_id:
        try:
            dev = MobileDevice.objects.get(id=int(dev_id), active=True)
        except:
            dev = None
            return
    if dev:
        if request.REQUEST.get('recalc_stops','') != '':
            RecalcStops()(device_id=dev.id, for_date=date.strftime('%Y-%m-%d'), force=True )

        header['department'] = str(dev.department)  # there will be group and get group full name
        header['tp_id'] = dev.user_id
        header['tp_fio'] = str(dev.user)
        header['tp_phone'] = dev.phone
        header['url'] = '{}#/Device/{}/{:%Y-%m-%d}'.format(app_url_path, dev.id, date)

        footer['distance']=0
        footer['avg_stop_time'] = 0
        footer['stops_time'] = 0
        footer['stops_count'] = 0

        last_stop_id = None
        prev_pnt = None
        last_timestamp = None
        time_step = int(request.REQUEST.get('time_step','10'))*60
        arc_distance=0
        timefilter['something_wrong'] = False
        point_set = dev.point_set.filter( **timefilter ).order_by('when_gathered')
        #print('point filter:',timefilter, point_set.count(), dev.id)
        footer['points_count'] = point_set.count()
        request_address = (request.REQUEST.get('no-address', '') == '')
        npp = 1
        for pnt in point_set:

            if prev_pnt:
                distance_to_prev = prev_pnt.distanceTo(pnt)
                footer['distance'] += distance_to_prev
                arc_distance += distance_to_prev

            pnt_timestamp = pnt.when_gathered.timestamp()
            time_diff = last_timestamp and (pnt_timestamp-last_timestamp)

            if not last_timestamp or time_diff >= time_step or prev_pnt.in_stop_id != pnt.in_stop_id:

                getted_address = request_address and (str(pnt.id)+'  --- ' +get_address(pnt.lat, pnt.lng)) or str(pnt)
                #getted_address += ' ### {}  :  {}'.format(pnt_timestamp, last_timestamp)
                table.append(dict(
                    npp=npp,
                    time=pnt.when_gathered.strftime('%d.%m.%Y-%H:%M:%S'),
                    distance=arc_distance and '{:7.2f}'.format(arc_distance),
                    #speed = (arc_distance/1000)/(time_diff/3600),
                    speed=arc_distance and time_diff and '{:7.2f}'.format(arc_distance*3.6/time_diff) or '-',
                    highlight=pnt.in_stop_id and True,
                    url=pnt.in_stop_id and '{}#/Stop/{}/{:%Y-%m-%d}'.format(app_url_path, pnt.in_stop_id, date) or '',
                    address=(pnt.in_stop and pnt.in_stop.address or getted_address),
                    time_diff=time_diff and hours2str(time_diff/3600) or '-'
                ))
                npp +=1

                last_timestamp = pnt_timestamp
                arc_distance = 0

            if pnt.in_stop_id is not None and pnt.in_stop_id != last_stop_id:
                print('stop==',last_stop_id,pnt.in_stop_id)
                last_stop_id = pnt.in_stop_id
                footer['stops_count'] += 1
                footer['stops_time'] += pnt.in_stop.how_long

            prev_pnt = pnt

        footer['distance'] = '{:7.2f} м'.format(footer['distance'])
        if footer['stops_count']>0:
            footer['avg_stop_time'] = footer['stops_time']/footer['stops_count']
        footer['avg_stop_time'] = hours2str(footer['avg_stop_time'])
        footer['stops_time'] = hours2str(footer['stops_time'])

    return dict(
        header=header,
        table=table,
        footer=footer
    )

# common_report = CommonReport()
# detail_report = DetailReport()

