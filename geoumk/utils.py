# -*- coding: utf-8 -*-
import time
import datetime
from django.core.serializers.json import json
from django.db.models.query import QuerySet
from geoumk.models import Point, StopPoint, MobileDevice
import threading
import urllib
__author__ = 'vvf'

GEO_DECODER_URL = 'http://10.10.55.48/nominatim/reverse.php'

def hours2str(h):
    h1 = int(h)
    m = (h-h1)*60
    m1 = int(m)
    sec = int((m-m1)*60)
    if h1 <= 0 and m1 <= 0:
        return '{} c'.format(sec)
    s = ''
    if h1:
        s += '{} ч '.format(h1)
    if m1:
        s += ' {} мин '.format(m1)
        return s or ' sec={}'.format(h*3600)


def bounds(lat1, lng1, lat2,lng2):
    if lat1>lat2:
        (lat1 , lat2) = (lat2, lat1)
    if lng1>lng2:
        (lng1 , lng2) = (lng2, lng1)
    return dict(lat__gte=lat1, lat__lte=lat2, lng__gte=lng1, lng__lte=lng2)

def create_date_filter( prefix, for_date=None, time_from=None, time_to=None):
    # TODO: check time_from and time_to is instance of datetime and convert to string
    timefilter = {
        # something_wrong=False,
        prefix+'__gt': time_from or datetime.datetime.today().strftime('%Y-%m-%d 00:00:00')
    }
    if time_to:
        timefilter[prefix+'__lt'] = time_to

    if for_date:
        if '.' in for_date:
            arr_date = for_date.split('.')
            #arr_date.reverse()
            for_date = '-'.join(arr_date)
        timefilter[prefix+'__gt'] = for_date+' 00:00:00'
        timefilter[prefix+'__lt'] = for_date+' 23:59:59'

    return timefilter


def get_address(lat, lng):
    content = urllib.request.urlopen('{}?format=json&lat={}&lon={}&zoom=18&addressdetails=1'.format(GEO_DECODER_URL, lat, lng))
    content_str = content.read().decode('utf8')
    address = json.loads( content_str )
    if 'display_name' in address:
        return address['display_name']
    if 'error' in address:
        return 'Error:' + address['error']
    raise ValueError('Invalid json:' + content_str)

class AddressResolverQueue:
    """
        defines queue of tasks to resolve address from stop points
    """

    queue = []
    worker = None

    class Worker(threading.Thread):
        queue = []
        def __init__(self, queue):
            self.queue = queue
            super(AddressResolverQueue.Worker, self).__init__(name='Address resolver')

        def run(self):

            print('resolver stared')

            while len(self.queue) > 0:
                # TODO: make it threadsafe
                # here lock changes of self.address_resolve_queue
                task_data = self.queue.pop()
                # here unlock changes of self.address_resolve_queue
                print('get address for point {}'.format(task_data.point_stop))
                task_data.point_stop.address = get_address(task_data.point_stop.lat,task_data.point_stop.lng)
                print('\taddress of {} is {}'.format(task_data.id, task_data.point_stop.address))
                task_data.point_stop.save()

                # here make request to service and get address:

    def enqueue(self, task_data):
        # TODO: threadsafe
        # here lock changes of self.address_resolve_queue
        #return
        if isinstance(task_data, list) or isinstance(task_data,tuple):
            for task_data_one in task_data:
                self.queue.insert(0, task_data_one)
        else:
            self.queue.insert(0, task_data)
        # here unlock changes of self.address_resolve_queue
        if not self.worker or not self.worker.is_alive():
            self.worker = self.Worker( self.queue )
            self.worker.start()


address_resolve_queue = AddressResolverQueue()

class RecalcStops:

    stop_points = []
    current_stop = None
    # default

    # при превышении этой дистанции от первоначальной точки останова будет создаваться новая точка останова.
    distance_for_new_stop = 700
    # скорость ниже которой считается что не двигается
    stop_speed = 6
    # Время движения которое не будет считаться движением если после него снова остановка (будет считаться что предыдущая остановка продолжается)
    min_move_time = 90
    last_stop = None
    max_acceleration=13.6  # g=9.8 m/sec/sec = 2.72 km/h/sec, 5g = 13.6km/h/sec
    last_speed = None

    # минимальное количество времени простоя в часах которая считается остановнкой
    min_hours_to_stop = 6/60  # 6 minutes

    def close_stop(self, point, saveAnyway=True):
        if self.current_stop:  # "close" last stop if it is
            self.current_stop.point_continue = point
            self.current_stop.point_set = self.stop_points
            self.current_stop.recalc_fields()
            print('***** try to close stop time={:9.3f} hours = {:9.3f} sec\n\t\tpoint from {} to {}'.format(self.current_stop.how_long, self.current_stop.how_long*3600,
                                                                                    self.current_stop.point_stop.id, point.id))
            if saveAnyway or self.current_stop.how_long > self.min_hours_to_stop:  # more than 6 minutes
                if self.current_stop.point_continue == None:
                    self.current_stop.point_continue = self.current_stop.point_stop
                self.current_stop.save()
                self.current_stop.point_set = self.stop_points  # there is points included in this stop. They mustn't to shown
                self.current_stop.save()
                print('::::: save stop:{0.id} {0.how_long} '.format(self.current_stop))
                if not self.current_stop.address:
                    address_resolve_queue.enqueue(self.current_stop)
            else:
                print('::::: drop stop started in point {} (it is too short to be a stop)'.format(self.current_stop.point_stop.id))
            self.stop_points = []
            self.last_stop = self.current_stop
            self.current_stop = None

    def __call__(self, *args, **kwargs):
        """
        Пересчитывает все не пересчитанные точки в остановки

        Если точка не на остановке - то какая предыдущая остановка была (откуда выехал):
            p.when_gathering
        """

        filter_params=dict(
            processed_stop=False
        )
        if 'device' in kwargs and kwargs['device']:
            filter_params['device_id'] = kwargs['device'].id
        if 'device_id' in kwargs and kwargs['device_id']:
            filter_params['device_id'] = kwargs['device_id']

        if filter_params.get('device_id') and isinstance(filter_params['device_id'],list) and len(filter_params['device_id'])>0:
            if isinstance(filter_params['device_id'][0], MobileDevice) :
                filter_params['device_id__in'] = [d.id for d in filter_params['device_id']]
            else:
                filter_params['device_id__in'] = filter_params['device_id']
            del filter_params['device_id']

        if filter_params.get('device_id') and isinstance(filter_params['device_id'], QuerySet):
            filter_params['device_id__in'] = [d['id'] for d in filter_params['device_id'].values('id')]
            del filter_params['device_id']

        # if force - delete stops for this device and for this date if it is
        if 'force' in kwargs and kwargs['force'] is True:
            del filter_params['processed_stop']
            stops_to_delete = StopPoint.objects.filter(**filter_params)
            if 'for_date' in kwargs:
                stops_to_delete = stops_to_delete.filter(
                    time_stop__gte= kwargs['for_date'],
                    time_stop__lte= kwargs['for_date'] + ' 23:59:59'
                )
            stops_to_delete.delete()


        if 'for_date' in kwargs:
            filter_params['when_gathered__gte'] = kwargs['for_date']
            filter_params['when_gathered__lte'] = kwargs['for_date'] + ' 23:59:59'

        # query points to recalc
        pnts_to_recalc = Point.objects.filter(**filter_params).order_by('device', 'when_gathered', 'id')
        if 'force' in kwargs and kwargs['force'] is True:
            pnts_to_recalc.update(something_wrong=False, in_stop=None, processed_stop=False, speed=None)
        # print('>>>>>>>>>>>>> args: {}'.format(kwargs))
        print('>>>>>>>>>>>>> start to recalc {} points filtred by: {}'.format(pnts_to_recalc.count(), filter_params))
        prev_point = None
        i=0
        prev_dev_id = -1
        self.current_stop = None
        moving_points = []
        # viewed_points = []
        last_speed = None
        for pnt in pnts_to_recalc:

            # while len(viewed_points) > 5:
            #     viewed_points.pop()
            # viewed_points.insert(0, pnt)

            if pnt.device_id != prev_dev_id:
                # Next device start
                self.close_stop(prev_point, True)
                prev_point = pnt.get_previous_point()
                print('———— new device {} at point {} ———'.format(pnt.device_id, pnt))

                if prev_point:
                    self.current_stop = prev_point.in_stop
                    print('———— prev point was in stop: staying')

                prev_dev_id = pnt.device_id
                moving_points = []
            distance = 0

            speed = pnt.speed or pnt.calcSpeed(prev_point, True)
            # calc distance
            if last_speed and speed:
                t_diff = pnt.timestampsDiff( prev_point )
                if t_diff > 1e-10:
                    acceleration = ( speed-last_speed ) / t_diff # km/h per seconds
                    if abs(acceleration) > self.max_acceleration:
                        print('!!! to fast speed-up: {:5.2f}km/h to {:5.2f}km/h by {:5.2f} sec'.format(last_speed, speed, t_diff))
                        pnt.something_wrong = True


            if not pnt.something_wrong and prev_point:
                distance = prev_point.distanceTo(pnt)

            if prev_point and (pnt.something_wrong or pnt.speed is None or pnt.speed>800 or distance > 10000):  # distance between points must be less than 10 km
                # first speed value can't show to wrong point - may be previous point is wrong, not current
                # first speed is wrong only next two points both are truly
                pnt.something_wrong=True
                pnt.processed_stop = True
                print( 'Ignore point {}'.format( pnt) )
                # pnt.speed = None
                pnt.save()
                if last_speed is None:
                    prev_point = pnt
                if kwargs.get('breakOnWrongPoint'):
                    return
                continue
            last_speed = speed
            is_pnt_in_stop = False
            # if speed grows more than stop_speed - it is moving start
            if isinstance(speed, float) and speed > self.stop_speed:
                if self.current_stop:
                    print('———— moving (speed up) at point {}:{} ———'.format(pnt.id, pnt))
                self.close_stop( pnt, False)
                moving_points.append( pnt )
            elif isinstance(speed, float):
                # speed calculated but it less than stop_speed
                if self.current_stop:
                    # already in stop
                    if self.current_stop.point_stop.distanceTo( pnt ) > self.distance_for_new_stop:
                        # close previous stop and begins a new stop
                        print('———— created another stop by distance. Stop point id={},  distance to pnt({})={}'
                              .format(self.current_stop.point_stop.id, pnt.id, self.current_stop.point_stop.distanceTo( pnt )))
                        self.close_stop( pnt, True )
                        self.current_stop = StopPoint.objects.filter(point_stop_id=prev_point.id).last() or \
                            StopPoint(point_stop=prev_point)
                else:

                    if self.last_stop and len( moving_points ) > 0 and pnt.when_gathered.timestamp() - moving_points[0].when_gathered.timestamp() < self.min_move_time:
                        print('**** moving was wrong: continue staying in previous stop point: time diff:{:5.2f} {}'.format( pnt.when_gathered.timestamp() - moving_points[0].when_gathered.timestamp(), self.last_stop ))
                        pnt.when_gathered.timestamp() - moving_points[0].when_gathered.timestamp()
                        pnt.when_gathered.timestamp() - moving_points[0].when_gathered.timestamp()
                        self.current_stop = self.last_stop
                        self.stop_points = list(self.current_stop.point_set.all()) + moving_points
                    else:
                        pnt_stop = prev_point or pnt
                        self.current_stop = StopPoint.objects.filter(point_stop_id=pnt_stop.id).last() or \
                            StopPoint(point_stop=pnt_stop)

                    print('———— new stop detected at point ({}) by speed: {:7.2f} ———'.format(pnt, speed or -1.111))
                moving_points=[]
                self.stop_points.append(pnt)
                is_pnt_in_stop = True
            pnt.processed_stop = True
            if prev_point:
                if 'debugPoints' in kwargs:
                    print('{}: {} => {:9.3f} meters , {:9.3f} km/h, {:9.3f} km/h, time:{:%H:%M:%S}'.format(pnt.id, 'staying' if is_pnt_in_stop else ' moving', distance or -1.111, float(pnt.speed or -1.111), float(speed or -1.111), pnt.when_gathered))
                    if distance <= 0.0000001 or pnt.speed is None:
                        print('\t{}\n\t{}'.format(prev_point, pnt))
            pnt.save()
            prev_point = pnt
            i += 1

        self.close_stop(prev_point, True)

recalc_stops = RecalcStops()

def recalc_paths():
    """
    Пересчет всех путей:
        - перевод точек в пути по дорогам,
        - рассчет расстояний,
        - привязка к точкам останова: из какой выехал и в какую приехал
    Добавляются новые точки в путь только если между точками растояние более ... и скорость в пределах допустимой.
    пересчитывается новое рассстояние по найденному маршруту и новая скорость, в соответствии с новой скоростью в добавляемые точки проставляется время.
    """
    pass
