# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MarkPoint'
        db.create_table('geoumk_markpoint', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lat', self.gf('django.db.models.fields.FloatField')(default=999.999)),
            ('lng', self.gf('django.db.models.fields.FloatField')(default=999.999)),
            ('radius', self.gf('django.db.models.fields.FloatField')(default=20.0)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal('geoumk', ['MarkPoint'])


    def backwards(self, orm):
        # Deleting model 'MarkPoint'
        db.delete_table('geoumk_markpoint')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'object_name': 'Permission', 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'blank': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'user_set'", 'to': "orm['auth.Group']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'user_set'", 'to': "orm['auth.Permission']", 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'db_table': "'django_content_type'", 'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType'},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'geoumk.branch': {
            'Meta': {'object_name': 'Branch'},
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.direction': {
            'Meta': {'object_name': 'Direction'},
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.group': {
            'Meta': {'object_name': 'Group'},
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.Branch']"}),
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'blank': 'True', 'null': 'True'}),
            'direction': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.Direction']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.markpoint': {
            'Meta': {'object_name': 'MarkPoint'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {'default': '999.999'}),
            'lng': ('django.db.models.fields.FloatField', [], {'default': '999.999'}),
            'radius': ('django.db.models.fields.FloatField', [], {'default': '20.0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'geoumk.mobiledevice': {
            'Meta': {'object_name': 'MobileDevice'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'blank': 'True', 'null': 'True'}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.Group']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'unique': 'True', 'blank': 'True', 'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.MobileUser']", 'blank': 'True'})
        },
        'geoumk.mobileuser': {
            'Meta': {'object_name': 'MobileUser'},
            'active': ('django.db.models.fields.BooleanField', [], {}),
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'blank': 'True', 'null': 'True'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.Group']"}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.point': {
            'Meta': {'object_name': 'Point'},
            'accuracy': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'null': 'True'}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geoumk.MobileDevice']"}),
            'heading': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_stop': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geoumk.StopPoint']", 'blank': 'True', 'null': 'True'}),
            'ip_address': ('django.db.models.fields.IPAddressField', [], {'default': "'127.0.0.1'", 'blank': 'True', 'max_length': '15'}),
            'lat': ('django.db.models.fields.FloatField', [], {'default': '999.999'}),
            'lng': ('django.db.models.fields.FloatField', [], {'default': '999.999'}),
            'processed_stop': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'something_wrong': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'speed': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'null': 'True'}),
            'when_delivered': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'when_gathered': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        'geoumk.stoppoint': {
            'Meta': {'object_name': 'StopPoint'},
            'address': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '220'}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geoumk.MobileDevice']"}),
            'how_long': ('django.db.models.fields.FloatField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'point_continue': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'stops_continue_set'", 'to': "orm['geoumk.Point']", 'unique': 'True'}),
            'point_stop': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['geoumk.Point']", 'unique': 'True'}),
            'time_continue': ('django.db.models.fields.DateTimeField', [], {}),
            'time_stop': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['geoumk']