# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Point.accuracy'
        db.add_column('geoumk_point', 'accuracy',
                      self.gf('django.db.models.fields.FloatField')(blank=True, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Point.accuracy'
        db.delete_column('geoumk_point', 'accuracy')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'object_name': 'Permission', 'unique_together': "(('content_type', 'codename'),)"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'blank': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Group']", 'related_name': "'user_set'", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'related_name': "'user_set'", 'symmetrical': 'False'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'object_name': 'ContentType', 'ordering': "('name',)", 'db_table': "'django_content_type'", 'unique_together': "(('app_label', 'model'),)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'geoumk.branch': {
            'Meta': {'object_name': 'Branch'},
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['auth.User']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.direction': {
            'Meta': {'object_name': 'Direction'},
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['auth.User']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.group': {
            'Meta': {'object_name': 'Group'},
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.Branch']"}),
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['auth.User']", 'null': 'True'}),
            'direction': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.Direction']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.mobiledevice': {
            'Meta': {'object_name': 'MobileDevice'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['auth.User']", 'null': 'True'}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'blank': 'True', 'to': "orm['geoumk.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100', 'unique': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'blank': 'True', 'to': "orm['geoumk.MobileUser']"})
        },
        'geoumk.mobileuser': {
            'Meta': {'object_name': 'MobileUser'},
            'active': ('django.db.models.fields.BooleanField', [], {}),
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['auth.User']", 'null': 'True'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.Group']"}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.point': {
            'Meta': {'object_name': 'Point'},
            'accuracy': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'null': 'True'}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geoumk.MobileDevice']"}),
            'heading': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_stop': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['geoumk.StopPoint']", 'null': 'True'}),
            'ip_address': ('django.db.models.fields.IPAddressField', [], {'default': "'127.0.0.1'", 'blank': 'True', 'max_length': '15'}),
            'lat': ('django.db.models.fields.FloatField', [], {'default': '999.999'}),
            'lng': ('django.db.models.fields.FloatField', [], {'default': '999.999'}),
            'processed_stop': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'something_wrong': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'speed': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'null': 'True'}),
            'when_delivered': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'when_gathered': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        'geoumk.stoppoint': {
            'Meta': {'object_name': 'StopPoint'},
            'address': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '220'}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geoumk.MobileDevice']"}),
            'how_long': ('django.db.models.fields.FloatField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'point_continue': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['geoumk.Point']", 'related_name': "'stops_continue_set'", 'unique': 'True'}),
            'point_stop': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['geoumk.Point']", 'unique': 'True'}),
            'time_continue': ('django.db.models.fields.DateTimeField', [], {}),
            'time_stop': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['geoumk']