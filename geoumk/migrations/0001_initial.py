# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Direction'
        db.create_table('geoumk_direction', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('change', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('changer', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['auth.User'], blank=True)),
        ))
        db.send_create_signal('geoumk', ['Direction'])

        # Adding model 'Branch'
        db.create_table('geoumk_branch', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('change', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('changer', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['auth.User'], blank=True)),
        ))
        db.send_create_signal('geoumk', ['Branch'])

        # Adding model 'Group'
        db.create_table('geoumk_group', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('direction', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['geoumk.Direction'])),
            ('branch', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['geoumk.Branch'])),
            ('change', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('changer', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['auth.User'], blank=True)),
        ))
        db.send_create_signal('geoumk', ['Group'])

        # Adding model 'MobileUser'
        db.create_table('geoumk_mobileuser', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('middle_name', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['geoumk.Group'])),
            ('change', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')()),
            ('changer', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['auth.User'], blank=True)),
        ))
        db.send_create_signal('geoumk', ['MobileUser'])

        # Adding model 'MobileDevice'
        db.create_table('geoumk_mobiledevice', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('imei', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('phone', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100, blank=True)),
            ('date_create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('change', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['geoumk.Group'], blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['geoumk.MobileUser'], blank=True)),
            ('changer', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['auth.User'], blank=True)),
        ))
        db.send_create_signal('geoumk', ['MobileDevice'])

        # Adding model 'Point'
        db.create_table('geoumk_point', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('device', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['geoumk.MobileDevice'])),
            ('lat', self.gf('django.db.models.fields.FloatField')(default=999.999)),
            ('lng', self.gf('django.db.models.fields.FloatField')(default=999.999)),
            ('speed', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('heading', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('ip_address', self.gf('django.db.models.fields.IPAddressField')(max_length=15, default='127.0.0.1', blank=True)),
            ('when_gathered', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('when_delivered', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('processed_stop', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('something_wrong', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('in_stop', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['geoumk.StopPoint'], blank=True)),
        ))
        db.send_create_signal('geoumk', ['Point'])

        # Adding model 'StopPoint'
        db.create_table('geoumk_stoppoint', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('point_stop', self.gf('django.db.models.fields.related.OneToOneField')(unique=True, to=orm['geoumk.Point'])),
            ('point_continue', self.gf('django.db.models.fields.related.OneToOneField')(unique=True, related_name='stops_continue_set', to=orm['geoumk.Point'])),
            ('device', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['geoumk.MobileDevice'])),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=220, blank=True)),
            ('how_long', self.gf('django.db.models.fields.FloatField')()),
            ('time_stop', self.gf('django.db.models.fields.DateTimeField')()),
            ('time_continue', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('geoumk', ['StopPoint'])


    def backwards(self, orm):
        # Deleting model 'Direction'
        db.delete_table('geoumk_direction')

        # Deleting model 'Branch'
        db.delete_table('geoumk_branch')

        # Deleting model 'Group'
        db.delete_table('geoumk_group')

        # Deleting model 'MobileUser'
        db.delete_table('geoumk_mobileuser')

        # Deleting model 'MobileDevice'
        db.delete_table('geoumk_mobiledevice')

        # Deleting model 'Point'
        db.delete_table('geoumk_point')

        # Deleting model 'StopPoint'
        db.delete_table('geoumk_stoppoint')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'unique_together': "(('content_type', 'codename'),)", 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'user_set'", 'to': "orm['auth.Group']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'user_set'", 'to': "orm['auth.Permission']", 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'object_name': 'ContentType', 'unique_together': "(('app_label', 'model'),)", 'ordering': "('name',)", 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'geoumk.branch': {
            'Meta': {'object_name': 'Branch'},
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['auth.User']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.direction': {
            'Meta': {'object_name': 'Direction'},
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['auth.User']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.group': {
            'Meta': {'object_name': 'Group'},
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.Branch']"}),
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['auth.User']", 'blank': 'True'}),
            'direction': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.Direction']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.mobiledevice': {
            'Meta': {'object_name': 'MobileDevice'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['auth.User']", 'blank': 'True'}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.Group']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.MobileUser']", 'blank': 'True'})
        },
        'geoumk.mobileuser': {
            'Meta': {'object_name': 'MobileUser'},
            'active': ('django.db.models.fields.BooleanField', [], {}),
            'change': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'changer': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['auth.User']", 'blank': 'True'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['geoumk.Group']"}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        'geoumk.point': {
            'Meta': {'object_name': 'Point'},
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geoumk.MobileDevice']"}),
            'heading': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_stop': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['geoumk.StopPoint']", 'blank': 'True'}),
            'ip_address': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'default': "'127.0.0.1'", 'blank': 'True'}),
            'lat': ('django.db.models.fields.FloatField', [], {'default': '999.999'}),
            'lng': ('django.db.models.fields.FloatField', [], {'default': '999.999'}),
            'processed_stop': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'something_wrong': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'speed': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'when_delivered': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'when_gathered': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        'geoumk.stoppoint': {
            'Meta': {'object_name': 'StopPoint'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '220', 'blank': 'True'}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geoumk.MobileDevice']"}),
            'how_long': ('django.db.models.fields.FloatField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'point_continue': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'related_name': "'stops_continue_set'", 'to': "orm['geoumk.Point']"}),
            'point_stop': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['geoumk.Point']"}),
            'time_continue': ('django.db.models.fields.DateTimeField', [], {}),
            'time_stop': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['geoumk']