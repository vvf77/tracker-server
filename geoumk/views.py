# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from geoumk import utils
from geoumk.utils import create_date_filter, hours2str, bounds
from .models import *
from django.http import HttpResponse
import math
import time
import datetime
import json
from .utils import RecalcStops


def get_departments(request):
    # Здесь в зависимости от прав ользователя:
    #    1 выдавать ему только те к ккоторым он имеет доступ
    #    2 сделать то что ему доступно - по умлочанию

    result = dict(success=True,
                    branches=[dict(id='all', name='Все филиалы')] + list(Branch.objects.all().values('id', 'name')),
                    directions=[dict(id='all', name='Все направления')] + list(Direction.objects.all().values('id', 'name'))
                  )

    return HttpResponse(json.dumps(result), content_type="application/json")

def get_devices(request, direction, branch):
    # dev_list = MobileDevice.objects.filter(active=True).order_by('department')
    # Возможно надо будет сделать чтобы выбирались по несколько направлений и несколько филиалов
    #  поэтому в название группы (подразделения) должен будет входить филиал если их выбрано несколько и направление если их выбрано несколько.
    filter = {}
    if branch != 'all':
        filter['branch_id'] = branch
    if direction != 'all':
        filter['direction_id'] = direction

    deps = Group.objects.filter(**filter).order_by('name')
    deps = [ {'id':d.id,'name':d.name,'devices':list(d.mobiledevice_set.filter(active=True).values('id','imei','user'))} for d in deps ]

    result = dict(success=True, list=deps)
    #TODO: deep, tagged cache
    #
    return HttpResponse(json.dumps(result), content_type="application/json")

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_device_path(request):
    """
    Параметры: ID устройства (id), период: или день в формате YYYY-MM-DD (fordate) или fromtime totime
     или ID точки останова (stop_id)
    Если не указано что продложать с... - то отдавать все точки за период за сегодня, иначе - только новые
    Если указан период  или день - то только за этот период/день. Тогда повторных запросов новых точек не должно быть.
    + Может быть передана ИД точки останова - взять из нее период (весь тот день) и устройство
    """

    result = {
        'path':{},
        'stops':[]
    }
    dev = None
    stop = None
    for_date =request.REQUEST.get('fordate', None)
    try:
        id = request.REQUEST.get('id')
        if id:
            dev = MobileDevice.objects.get(id=int(id))
        else:
            stop_id = request.REQUEST.get('stop_id')
            if stop_id:
                stop = StopPoint.objects.get(id=int(stop_id))
                id = stop.device_id
                dev = stop.device
                for_date = stop.time_stop.strftime('%Y-%m-%d')

    except ObjectDoesNotExist:
        pass

    if not dev:
        return HttpResponse(json.dumps(result), content_type="application/json")

    result['device_id'] = dev.id
    result['department_id'] = dev.department_id

    timefilter = create_date_filter('when_gathered',
                                    for_date=for_date,
                                    time_from=request.REQUEST.get('fromtime'),
                                    time_to=request.REQUEST.get('totime'))

    if for_date:
        if '.' in for_date:
            arr_date = for_date.split('.')
            #arr_date.reverse()
            for_date = '-'.join(arr_date)
        result['for_date'] = for_date

    #timefilter.update( )

    current_stop = None
    stops_ids = []
    arc_pnts = []
    speed_sum = 0
    start_pnt = None
    last_pnt = None
    distance = 0

    # на всякий случай просчитываем точки остановок. Но потом можно будет это убрать после того как будет отдельный проесс для этого
    # RecalcStops()(device_id=dev.id)

    def next_stop(arc_pnts, distance, speed_sum, current_stop, pnt_stop ):
        # speed_avg = speed_sum / len(arc_pnts)
        time_diff = arc_pnts[-1]['when_gathered'] - arc_pnts[0]['when_gathered']
        time_diff = time_diff / 3600
        if time_diff:
            speed_avg2 = distance / 1000 / time_diff
        else:
            speed_avg2 = -1.001

        result['path']['s{}'.format(current_stop or '_start')]=dict(
            type = 'polyline',
            speed_avg = speed_sum,
            message = 'avg speed: {:7.3f} km/h or {:7.3f} km/h<br>distance={:7.3f}km, time={} <br>from {} to {}<br>point count={}'.format(
                speed_sum, speed_avg2,
                distance / 1000, hours2str( time_diff ),
                current_stop, pnt_stop, len(arc_pnts)),
            latlngs=arc_pnts)

    start_pnt_stop = None
    all_points = dev.point_set.filter(something_wrong=False, **timefilter).order_by('when_gathered','id').values('lat', 'lng', 'speed', 'stoppoint', 'in_stop', 'when_gathered', 'id')
    full_path = []

    for pnt in all_points:

        pnt['when_gathered'] = pnt['when_gathered'].timestamp()
        pnt_stop = pnt['in_stop'] or pnt['stoppoint']

        if not start_pnt:
            start_pnt = pnt
            start_pnt_stop = pnt_stop

        if pnt_stop and pnt_stop != current_stop:
            stops_ids.append( pnt_stop )
            if len(arc_pnts) > 0:
                next_stop(arc_pnts, distance, speed_sum, current_stop, pnt_stop )
            current_stop = pnt_stop
            if last_pnt:
                arc_pnts = [last_pnt]
            else:
                arc_pnts = []
            speed_sum = 0.0
            distance = 0.0
        #elif not pnt_stop:
        arc_pnts.append( pnt )

        if pnt['speed']:
            speed_sum = (speed_sum + pnt['speed']) / 2 if speed_sum > 0 else pnt['speed']

        del pnt['stoppoint']
        del pnt['in_stop']
        del pnt['speed']
        if last_pnt:
            distance += Point(**pnt).distanceTo( Point(**last_pnt) )
        full_path.append(pnt)
        last_pnt = pnt

    result['path']['all'] = dict(
            type = 'polyline',
            color = 'red',
            message = 'full path',
            latlngs=full_path)

    if len(arc_pnts)>0:
        next_stop(arc_pnts, distance, speed_sum, current_stop, pnt_stop )
        # result['path']['s{}'.format(current_stop or '_start')]=dict(
        #     type = 'polyline',
        #     speed_avg = speed_sum,
        #     latlngs=arc_pnts)

    if start_pnt:
        if not last_pnt:
            start_pnt = last_pnt
        # start_pnt['message'] = 'Last point for this day'
        start_pnt['first_position'] = True
        start_pnt['time_stop'] = datetime.datetime.fromtimestamp(start_pnt['when_gathered']).strftime('%H:%M:%S')  #%Y-%m-%d
        start_pnt['how_long'] = 'First point pnt#{}'.format(start_pnt['id'])
        start_pnt['address'] = utils.get_address(start_pnt['lat'], start_pnt['lng'])
        start_pnt['id'] = 0

        # if start point included into stop - than don't add it as start
        if not start_pnt_stop:
            result['stops'] = [start_pnt]

    stop_prev_date = None
    for stop in StopPoint.objects.filter( id__in = stops_ids ).order_by('id'):
        if not stop.address:
            stop.address = utils.get_address(stop.point_stop.lat, stop.point_stop.lng)
        time_stop = stop.time_stop.strftime('%H:%M:%S')
        # if stop_prev_date and stop_prev_date !=stop.time_stop.strftime('%Y-%m-%d'):
        time_stop = stop.time_stop.strftime('%d.%m ')+ time_stop
        stop_prev_date = stop.time_stop.strftime('%Y-%m-%d')
        stop_info = dict(
            time_stop=time_stop,  #%Y-%m-%d
            how_long=hours2str(stop.how_long),
            address=stop.address,
            lat=stop.point_stop.lat,
            lng=stop.point_stop.lng,
            id=stop.id
            )
        # stop_info['message'] = '{id} : {how_long} c {time_stop} {address}'.format(**stop_info)
        result['stops'].append(stop_info)

    if last_pnt:
        # last_pnt['message'] = 'Last point for this day'
        last_pnt['last_position'] = True
        last_pnt['how_long'] = 'Last point pnt#{}'.format(last_pnt['id'])
        last_pnt['time_stop'] = datetime.datetime.fromtimestamp(last_pnt['when_gathered']).strftime('%H:%M:%S')  #%Y-%m-%d
        last_pnt['address'] = utils.get_address(last_pnt['lat'], last_pnt['lng'])
        result['stops'].append(last_pnt)
        last_pnt['id'] = 999999999999

    result['success'] = True
    #result['path'] = {'all': result['path']['all']}
    result['timefilter'] = timefilter
    return HttpResponse(json.dumps(result), content_type="application/json")

########################################################################################
########################################################################################


def get_device_points(request):
    """
    Возвращает текущее положение перечисленных устройств
    """
    ids = request.REQUEST.get('id')
    if not ids:
        return HttpResponse('',  content_type="application/json")

    params = dict()
    last_timestamp = 0
    if request.REQUEST.get('ts'):
        last_timestamp = int(request.REQUEST.get('ts'))
        params['when_gathered__gt'] = datetime.datetime.fromtimestamp( last_timestamp )

    if request.REQUEST.get('bounds'):
        bounds_params = request.REQUEST.get('bounds')
        bounds_params = json.loads(bounds_params)
        bnd = bounds(**bounds_params)
        print( bnd )
        params.update( bnd )

    #points = Point.objects.filter(**params).order_by('when_gathered')
    result_data = dict(
        devices=[]
    )

    for dev in MobileDevice.objects.filter( id__in=[int(dev_id) for dev_id in ids.split(",")] ):

        pnt = dev.point_set.filter(**params).order_by('when_gathered').last()
        if pnt:

            if pnt.speed is None:
              pnt.calcSpeed()

            pnt_ts = pnt.when_gathered.timestamp()
            result_data['devices'].append({
                "point_id": pnt.id,
                "id": pnt.device_id,
                "lat": float(pnt.lat), "lng": float(pnt.lng),
                "speed": pnt.speed,
                "title": '{} <br> speed= {:5.1f} km/h'.format(dev.imei, pnt.speed or 0.000001),
                "time": pnt.when_gathered.strftime('%Y-%m-%d %H:%M:%S'),
                "when_gathered": pnt_ts,
            })
            if last_timestamp < pnt_ts:
                last_timestamp = pnt_ts
    result_data['last_timestamp'] = int(last_timestamp)

    return HttpResponse(json.dumps(result_data), content_type='application/json')


#---------------------------------------------------------------------------------------
month_names2num = dict([(datetime.datetime(2014,m,23).strftime('%b').lower(),m) for m in range(1,12)])

def add_device_position(request):
    """
    Добавляет точку - текущее положение устройства.
    """

    if request.REQUEST.get('imei','') != '' and request.REQUEST.get('lat') != '' and request.REQUEST.get('lon') != '' :
      imei, lat, lng = [ request.REQUEST[i] for i in ('imei', 'lat', 'lon') ]
      lat = lat.replace(',','.')
      lng = lng.replace(',','.')
      try:
          du = MobileDevice.objects.get(imei=imei)
      except MobileDevice.DoesNotExist:
          print(imei)
          print('create device')
          du = MobileDevice.objects.create(imei=imei)
          print( du )
          # TODO: направить уведомление о новом устройстве.

      p=Point(device=du, lat=lat, lng=lng, ip_address=get_client_ip( request ))
      if request.REQUEST.get('time', '') != '':
        #try:
          (day,hour,minute, second) = request.REQUEST.get('time', '').split(':')
          (mday, month, year) = day.split('/')
          when = datetime.datetime(int(year), month_names2num[month.lower()], int(mday), int(hour), int(minute), int(second))
          #when = datetime.datetime.strptime(request.REQUEST.get('time',''), '%d/%p/%Y:%Y:%M:%S')
          p.when_gathered = when
          # print(when)
          # print(when.timestamp())
          p.when_delivered = when

      if request.REQUEST.get('timestamp', '') != '':
        #try:
          p.when_delivered = datetime.datetime.fromtimestamp( float(request.REQUEST.get('timestamp', '')))
        #except:
        #  pass
      if request.REQUEST.get('accuracy','') != '':
          p.accuracy = int(request.REQUEST.get('accuracy',''))

      if request.REQUEST.get('ipadr','') != '':
          p.ip_address = request.REQUEST.get('ipadr','')

      if request.REQUEST.get('speed','') != '':
          p.speed = request.REQUEST.get('speed','')

      if request.REQUEST.get('heading','') != '':
          p.heading = request.REQUEST.get('heading','')

      p.save()

      return HttpResponse('1')
    else:
      return HttpResponse("0")


#---------------------------------------------------------------------------------------
