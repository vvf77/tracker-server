from django.test import TestCase
from django.test.client import Client
from geoumk import utils
from geoumk.models import *
import json
import datetime

# Create your tests here.
class TestCommon( TestCase ):
    fixtures = ['geoumk/fixtures/data','geoumk/fixtures/test']

    def setUp(self):
        self.client = Client()

    # def test_add_point(self):
    #     """
    #     После того как приходят данные от новго устройства - оно должно появится в базе
    #        и новая точка должа быть занесена в БД
    #     """
    #     response = self.client.get('/geoumk/event/addpoint/?imei=7xx123445&lat=33.4455&lon=44.332211')
    #     self.assertEqual(response.status_code, 200)
    #     #print( MobileDevice.objects.all().values() )
    #     dev = MobileDevice.objects.get( imei='7xx123445')  # must pass (exists)
    #     points = dev.point_set.filter(lat='33.4455', lon='44.332211')
    #     self.assertTrue(points.count() > 0)
    #
    # def test_get_points_and_speed(self):
    #     """
    #     После того как приходят данные от новго устройства - оно должно появится в базе
    #        и новая точка должа быть занесена в БД
    #     """
    #     response = self.client.get('/geoumk/event/addpoint/', dict(imei='89888694656',lat='45.04465858',lon='38.96857148',time='22/Sep/2014:06:25:06'))
    #     response = self.client.get('/geoumk/event/addpoint/?imei=89888694656&lat=45,04144114&lon=38.96760588&time=22/Sep/2014:06:25:26')
    #     dev = MobileDevice.objects.get( imei='89888694656')  # must pass (exists)
    #     response = self.client.get('/geoumk/event/getpoint/', dict(id=dev.id, debug=1))
    #     self.assertEqual(response.status_code, 200)
    #
    #     print(str(response.content))
    #     pointData = json.loads(response.content.decode())
    #     self.assertEqual(int(pointData['points'][0]['speed']), 65)
    #
    #     dev = MobileDevice.objects.create(imei='test123', phone='test123')
    #     response = self.client.get('/geoumk/event/getpoint/', {'id':dev.id})
    #     print(response.content)


        #print( MobileDevice.objects.all().values() )

    def test_stops(self):
        """
        - загрузить ряд точек,
        - запупстить пересчет
        - проверить правильность обнаружения остановок
        """
        dev = MobileDevice.objects.create(imei='+79615159799')
        createTestPoints(dev.id)
        utils.recalc_stops()
        num = 0
        for pitstop in StopPoint.objects.filter(device=dev):
            near_lng, near_lat =  stops_near[num]
            must_be_near = Point(lat=near_lat,lng=near_lng, device_id=dev.id)
            dist_start = must_be_near.distanceTo(pitstop.point_stop)
            dist_end = must_be_near.distanceTo(pitstop.point_continue)
            # print('distance from "must be" point: {} and {} meters'.format(dist_start,dist_end))
            # if dist_start>20 or dist_end>20:
            #     print('points are: {}, {}'.format(pitstop.point_stop, pitstop.point_continue))
            # self.assertLess(dist_start, 20)
            # self.assertLess(dist_end,   20)
            num += 1

    #def test_get_path(self):
        """
        После того как приходят данные от новго устройства - оно должно появится в базе
           и новая точка должа быть занесена в БД
        """
        response = self.client.get('/geoumk/event/addpoint/', dict(imei='89888694656',lat='45.04465858',lng='38.96857148',time='22/Sep/2014:06:25:06'))
        response = self.client.get('/geoumk/event/addpoint/?imei=89888694656&lat=45,04144114&lng=38.96760588&time=22/Sep/2014:06:25:26')
        dev = MobileDevice.objects.get( imei='89888694656')  # must pass (exists)
        response = self.client.get('/geoumk/event/getpoint/', dict(id=dev.id, debug=1))
        self.assertEqual(response.status_code, 200)

        print(str(response.content))
        pointData = json.loads(response.content.decode())
        self.assertEqual(int(pointData['points'][0]['speed']), 65)

        # dev = MobileDevice.objects.create(imei='test123', phone='test123')
        dev = MobileDevice.objects.get(imei='+79615159799')
        # createTestPoints( dev.id )
        pnts = dev.point_set.filter( something_wrong=False,
            when_gathered__gt=datetime.datetime.today().strftime('%Y-%m-%d'))

        print('----------- stops={}\n\tpnts = {}'.format(dev.stops_set.count(), pnts.count() ))
        response = self.client.get('/geoumk/event/getline/', {'id':dev.id})
        print(response.content)

stops_near = [
             [39.00175452232361,  45.062252955506075],
             [39.0019690990448,   45.062783426026684],
             [39.00173306465149,  45.064503632282175],
             [39.001813530921936, 45.062313580957560],
             [39.00249481201172,  45.055246493780835],
             [39.016603231430054, 45.055644395887580]]

def createTestPoints(device_id):
    coordinates = [
          [
            38.99966239929199,
            45.06219990818331
          ],
          [
            39.00137901306152,
            45.06227569005791
          ],
          [
            39.0017008781433,
            45.062251060959674
          ],
          [
            39.00175452232361,
            45.06225674459868
          ],
          [
            39.00177597999573,
            45.06225863914487
          ],
          [
            39.00199323892593,
            45.062273795512276
          ],
          [
            39.001934230327606,
            45.06276448073569
          ],
          [
            39.001934230327606,
            45.06278532055543
          ],
          [
            39.00224804878235,
            45.0628042658395
          ],
          [
            39.00223731994629,
            45.06285352354872
          ],
          [
            39.00225341320038,
            45.06293498812852
          ],
          [
            39.002307057380676,
            45.06290657026502
          ],
          [
            39.002360701560974,
            45.062866785232394
          ],
          [
            39.002328515052795,
            45.0628288946994
          ],
          [
            39.00238752365112,
            45.06276448073569
          ],
          [
            39.00239288806915,
            45.06267543778398
          ],
          [
            39.00234192609787,
            45.062701961230914
          ],
          [
            39.002339243888855,
            45.062650808857974
          ],
          [
            39.00227487087249,
            45.06265459792421
          ],
          [
            39.00224804878235,
            45.06279668772662
          ],
          [
            39.002108573913574,
            45.062770164323645
          ],
          [
            39.00194764137268,
            45.06275500808785
          ],
          [
            39.00173038244247,
            45.06450173781035
          ],
          [
            39.00154262781143,
            45.06448089861631
          ],
          [
            39.001808166503906,
            45.06233252639794
          ],
          [
            39.00181084871292,
            45.06229842460069
          ],
          [
            39.00181621313095,
            45.06227569005791
          ],
          [
            39.00181084871292,
            45.062285162785166
          ],
          [
            39.00216221809387,
            45.058683518738114
          ],
          [
            39.00249481201172,
            45.05536018037958
          ],
          [
            39.002497494220734,
            45.05534123262882
          ],
          [
            39.00250017642975,
            45.05532417964776
          ],
          [
            39.00250017642975,
            45.05530144233179
          ],
          [
            39.002497494220734,
            45.05528249456157
          ],
          [
            39.00250017642975,
            45.055261652007054
          ],
          [
            39.00250017642975,
            45.05524270422367
          ],
          [
            39.002827405929565,
            45.05503806776283
          ],
          [
            39.0164852142334,
            45.05563681682572
          ],
          [
            39.01663541793823,
            45.05562165869894
          ],
          [
            39.01677489280701,
            45.05558376336443
          ],
          [
            39.017064571380615,
            45.05540186540914
          ],
          [
            39.01767611503601,
            45.05079359088226
          ]
        ]
    one_second = datetime.timedelta(0,1,0)
    point_time = datetime.datetime.now() - datetime.timedelta(0,1+len(coordinates),0)
    for lng, lat in coordinates:
        Point.objects.create(lat=lat, lng=lng, when_gathered=point_time, device_id=device_id)
        point_time += one_second
