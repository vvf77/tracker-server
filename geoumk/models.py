# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist

from django.db import models
from django.contrib.auth import models as auth_models
import math
from datetime import datetime

# TODO: create common class with common field change and changer, also do it filling
# Meta abstract = True
import south.signals


class Direction (models.Model):
    class Meta:
        verbose_name = 'Направление'
        verbose_name_plural = 'Направления'

    name = models.CharField('Название направления', max_length=127)
    change = models.DateTimeField('Дата изменения', auto_now=True)

    changer = models.ForeignKey(auth_models.User, null=True, blank=True, editable=False)

    def __str__(self):
        return self.name

    @staticmethod
    def getDefaultItem():
        try:
            default = Direction.objects.get(id=0)
        except ObjectDoesNotExist:
            default = Direction.objects.create(id=0,name='Не распределено')
        return default



class Branch (models.Model):
    class Meta:
        verbose_name = 'Филиал'
        verbose_name_plural = 'Филиалы'

    name = models.CharField('Название филиала', max_length=127)
    change = models.DateTimeField('Дата изменения', auto_now=True)

    changer = models.ForeignKey(auth_models.User, null=True, blank=True, editable=False)

    def __str__(self):
        return self.name
    @staticmethod
    def getDefaultItem():
        try:
            default = Branch.objects.get(id=0)
        except ObjectDoesNotExist:
            default = Branch.objects.create(id=0,name='Не распределено')
        return default

class Group(models.Model):
    class Meta:
        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'

    name = models.CharField('Название подразделения', max_length=127)
    direction = models.ForeignKey( Direction, verbose_name='Направление', default=0 )
    branch = models.ForeignKey( Branch, verbose_name='Филиал', default=0 )

    one_c_id = models.CharField('ИД подразделения в системе 1C ЮМК', max_length=50, blank=True)

    change = models.DateTimeField('Дата изменения', auto_now=True)

    changer = models.ForeignKey(auth_models.User, null=True, blank=True)

    @staticmethod
    def getDefaultItem():
        try:
            default = Group.objects.get(id=0)
        except ObjectDoesNotExist:
            default = Group.objects.create(id=0,name='Не распределено')
        return default

    def __str__(self):
        return self.name

class MobileUser( models.Model ):
    class Meta:
        verbose_name = 'Торговый представитель'
        verbose_name_plural = 'Торговые представители'

    # user of mobile device
    first_name = models.CharField('Имя', max_length=127)
    middle_name = models.CharField('Отчество', max_length=127)
    last_name = models.CharField('Фамилия', max_length=127)

    department = models.ForeignKey( Group, verbose_name='Подразделение', default=0)
    change = models.DateTimeField('Дата изменения', auto_now=True)
    active = models.BooleanField('Действующий')

    changer = models.ForeignKey(auth_models.User, null=True, blank=True)
    one_c_id = models.CharField('ИД пользователя в 1С ЮМК', max_length=50, blank=True)

    def __str__(self):
        return '{} {} {}'.format(self.first_name, self.last_name, self.middle_name)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        for dev in self.mobiledevice_set.exclude(department_id=self.department_id):
            dev.department_id = self.department_id
            dev.save()
        # UPDATE geoumk_mobiledevice d
        #    SET department_id={self.department_id}
        #  WHERE department_id<>{self.department_id} AND u.id= d.user_id;
        return super(MobileUser, self).save(force_insert, force_update, using, update_fields)

    @staticmethod
    def getDefaultItem():
        try:
            default = MobileUser.objects.get(id=0)
        except ObjectDoesNotExist:
            default = MobileUser.objects.create(id=0,first_name='никто',middle_name='никтоевич',last_name='ничей',active=True)
        return default


class MobileDevice( models.Model ):
    class Meta:
        verbose_name = 'Планшет/телефон'
        verbose_name_plural = 'Планшеты и телефоны'

    imei = models.CharField('IMEI', max_length=100, unique=True)
    phone = models.CharField('Номер телефона', max_length=100, unique=True, blank=True)
    #TODO:
    # model = models.CharField('Model', max_length=200 )
    # serial_number  = models.CharField('Серийный номер', max_length=100)
    # os_type  = models.ChoiceField('OS type', max_length=100)
    # os_version  = models.CharField('OS version', max_length=100)
    date_create = models.DateTimeField('Дата создания', auto_now_add=True)
    change = models.DateTimeField('Дата изменения', auto_now=True)

    department = models.ForeignKey(Group, verbose_name='Подразделение', blank=True, default=0)

    active = models.BooleanField('Действующий', default=True)
    user = models.ForeignKey(MobileUser, verbose_name='У пользователя', blank=True, default=0)

    changer = models.ForeignKey(auth_models.User, null=True, blank=True, editable=False)

    def __str__(self):
        return '{}'.format(self.imei or self.phone)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        self.department = self.user.department
        if not self.phone:
            self.phone = self.imei
        return super(MobileDevice, self).save(force_insert, force_update, using, update_fields)


class Point(models.Model):
    class Meta:
        ordering = ['when_gathered', 'id']

    device = models.ForeignKey(MobileDevice)
    #TODO: change coordinates type to decimal field (or to geometry-point)
    lat = models.FloatField(default=999.999)
    lng = models.FloatField(default=999.999)
        #.DecimalField(max_digits=25, decimal_places=20)
    # lat = models.CharField(max_length=25)
    # lon = models.CharField(max_length=25)
    speed = models.FloatField("Скорость", blank=True, null=True, editable=False)
    heading = models.FloatField("Направление (азимут)", blank=True, null=True, editable=False)
    ip_address = models.IPAddressField(blank=True, default='127.0.0.1')
    when_gathered = models.DateTimeField(default=datetime.now)
    when_delivered = models.DateTimeField(auto_now=True)

    accuracy = models.FloatField('Радиус погрешности (метр)',blank=True, null=True, editable=False)

    processed_stop = models.BooleanField(default=False, blank=False, null=False)
    something_wrong = models.BooleanField(default=False, blank=False, null=False)
    in_stop = models.ForeignKey('StopPoint', blank=True, null=True)
    _known_distances = {}

    def __str__(self):
        return 'ID:{} coord({}, {}) {}'.format(self.id, self.lat, self.lng, self.speed)

    def distanceTo(self, point2 ):
        # using earthdistance extention of posgres
        # SELECT earth_distance(ll_to_earth(p1.lat, p1.lng), ll_to_earth(p2.lat, p2.lng)),
        #   - is near but not accurate (about 0.084% mistake) http://geojson.io/ - tells earth_distance is more accuarate than this method
	    # (point(p1.lat, p1.lng) <@> point(p2.lat, p2.lng))*2090.3749807498
        #   operator gives result in miles but here mile mus be equal to 2090.3749807498 meters
        #  FROM geoumk_point p1, geoumk_point p2

        #pi - число pi, rad - радиус сферы (Земли)
        if point2.id in self._known_distances:
            return self._known_distances[point2.id]

        if not self._known_distances:
            self._known_distances = {}
        rad = 6372795
        # rad = 6378168
        # rad = 6371210

        #координаты двух точек
        llat1 = float(self.lat)
        llngg1 = float(self.lng)

        llat2 = float(point2.lat)
        llngg2 = float(point2.lng)
        if llat1 - llat2 <=0.000001 and llngg1 - llngg2 <=0.000001:
            return 0

        #в радианах
        lat1 = llat1*math.pi/180.
        lat2 = llat2*math.pi/180.
        lngg1 = llngg1*math.pi/180.
        lngg2 = llngg2*math.pi/180.

        #косинусы и синусы широт и разницы долгот
        cl1 = math.cos(lat1)
        cl2 = math.cos(lat2)
        sl1 = math.sin(lat1)
        sl2 = math.sin(lat2)
        delta = lngg2 - lngg1
        cdelta = math.cos(delta)
        sdelta = math.sin(delta)

        #вычисления длины большого круга
        y = math.sqrt(math.pow(cl2*sdelta,2)+math.pow(cl1*sl2-sl1*cl2*cdelta,2))
        x = sl1*sl2+cl1*cl2*cdelta
        ad = math.atan2(y,x)
        dist = ad*rad
        self._known_distances[ point2.id ] = dist  # Cache calculation results
        return dist

    def get_previous_point(self, only_correct=True):
        filter=dict(device_id=self.device_id, id__lt=self.id)
        if only_correct:
            filter['something_wrong']=False
        return Point.objects.filter(**filter).order_by('when_gathered', 'id').last()

    def timestampsDiff(self,point2):
        return self.when_gathered.timestamp() - point2.when_gathered.timestamp()

    def calcSpeed(self, point2=None, is_prev_point=False):
        if point2 == None:
          time_diff = 0
          while point2 == None and time_diff<0.1:
            is_prev_point = True
            point2 = self.get_previous_point()
            #Point.objects.filter(device_id=self.device_id, id__lt=self.id).order_by('when_gathered').last()
            if point2 == None:
                # TODO: найти ситуации когда сюда попадаем и выяснить как их отрабатывать
                print('Point.calcSpeed: unknown previous point of {}'.format(self.id))
                return -2.0
            time_diff = self.when_gathered.timestamp() - point2.when_gathered.timestamp()
        else:
          time_diff = self.when_gathered.timestamp() - point2.when_gathered.timestamp()
        #

        self.time_diff = time_diff
        if time_diff<0.01:
            if is_prev_point:
                self.something_wrong = True
            print('Point.calcSpeed: infinity speed: {} - {} =  {}'.format( self.when_gathered.timestamp(), point2.when_gathered.timestamp(), time_diff))
            return -1.0

        distance = self.distanceTo(point2) # meters
        # print('| meters =\t{}\n| seconds =\t{}\n| kmetrs =\t{}\n| hours =\t{}'.format(
        #     distance,time_diff,distance/1000,time_diff/3600
        # ))

        speed = distance*3.6/time_diff # kilometers per hour

        if is_prev_point:
            if speed > 300:
                self.something_wrong = True
            self.speed = speed
        return speed


class StopPoint(models.Model):
    # TODO: rename it to stop (without 's')
    point_stop = models.OneToOneField(Point)
    point_continue = models.OneToOneField(Point, related_name='stops_continue_set')
    device = models.ForeignKey(MobileDevice)
    address = models.CharField('Адрес', max_length=220, blank=True)
    how_long = models.FloatField()
    time_stop = models.DateTimeField()
    time_continue = models.DateTimeField()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if self.how_long == None or force_update or force_insert or self.device_id == None:
            self.recalc_fields()
        # print('stop.device_id={}'.format(self.device_id))
        return super(StopPoint, self).save(force_insert, force_update, using, update_fields)

    def recalc_fields(self):
        self.time_continue = self.point_continue.when_gathered
        self.time_stop = self.point_stop.when_gathered
        self.how_long = (self.time_continue.timestamp() - self.time_stop.timestamp())/3600
        if self.how_long < 0:
            print('Warning: wrong point sequence')
            self.time_continue, self.time_stop = self.time_stop, self.time_continue
            self.point_stop, self.point_continue = self.point_continue, self.point_stop
            self.how_long = -self.how_long
        self.device_id = self.point_stop.device_id


class MarkPoint(models.Model):
    class Meta:
        verbose_name = 'Метка'
        verbose_name_plural = 'Метки'
    CHOICES_MARK_TYPE = (
        ('home', 'Место жительства (дом)'),
        ('office', 'Офис (место работы)'),
        ('sale-point', 'Точка продаж'),
        ('entertainment', 'Развлекательное (ТРЦ)'),
    )
    lat = models.FloatField(default=999.999)
    lng = models.FloatField(default=999.999)
    radius = models.FloatField(default=20.0)
    title = models.CharField('Текст метки', max_length=128)
    type = models.CharField('Тип', max_length=128, choices=CHOICES_MARK_TYPE)

def check_and_create_defaults(app, **kwargs):
    Direction.getDefaultItem()
    Branch.getDefaultItem()
    Group.getDefaultItem()

south.signals.post_migrate.connect( check_and_create_defaults )
