# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

#from . import views

urlpatterns = patterns('',

    url(r'^json/departments$', 'geoumk.views.get_departments', name='deps'),
    url(r'^json/devices$', 'geoumk.views.get_devices', name='devices'),

#-----------------------------------------------------------------------------------------

    url(r'^event/addpoint/$', 'geoumk.views.add_device_position', name='addpoint'), # http://127.0.0.1:8000/geoumk/event/addpoint/?imei=123445&lat=1&lon=1
    #
    # url(r'^event/getpointexcel/$', views.PointGetExcel, name='addpoint'), # http://127.0.0.1:8000/geoumk/event/addpoint/?imei=123445&lat=1&lon=1
    #
    # url(r'^event/linegetexcel/$', views.LineGetExcel, name='linegetexcel'), #
    #
    url(r'^event/getpoint/$', 'geoumk.views.get_device_points', name='getpoint'), # http://127.0.0.1:8000/geoumk/event/getpoint/?id=1
    #
    url(r'^event/getline/$', 'geoumk.views.get_device_path', name='getline'), # http://127.0.0.1:8000/geoumk/event/getline/?id=1


    url(r'^api/departments/$', 'geoumk.views.get_departments', name='departments'),
    url(r'^api/devices/(\d+|all)?/?(\d+|all)?/?$', 'geoumk.views.get_devices', name='devices'),
    url(r'^api/getpoint/$', 'geoumk.views.get_device_points', name='getpoint'), # http://127.0.0.1:8000/geoumk/event/getpoint/?id=1
    url(r'^api/getline/$', 'geoumk.views.get_device_path', name='getline'), # http://127.0.0.1:8000/geoumk/event/getline/?id=1

    url(r'^report/common\.(?P<response_type>json|html|xls|csv|txt)','geoumk.reports.common_report', name='common_report'),
    url(r'^report/detail/(?P<dev_id>\d+)\.(?P<response_type>json|html|xls|csv|txt)','geoumk.reports.detail_report', name='detail_report'),

)