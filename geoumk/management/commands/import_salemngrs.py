from django.core.exceptions import ObjectDoesNotExist
from geoumk.models import Branch, Direction, Group, MobileUser

__author__ = 'vvf'

from optparse import make_option
from django.core.management.base import BaseCommand, CommandError, NoArgsCommand

# import_salemngrs

class Command( BaseCommand ):
    option_list = BaseCommand.option_list + (
        make_option('--force', '-f', dest='force', default=False, action="store_true",
            help='Force'),

    )
    help = 'Help text goes here'
    def create_or_get(self, cls, name):
        try:
            return cls.objects.get(name=name)
        except ObjectDoesNotExist:
            return cls.objects.create(name=name)

    def handle(self, filename, force=False, **options):
        if force:
            print('delete all')
            Branch.objects.exclude(id=0).delete()
            Direction.objects.exclude(id=0).delete()
        print("Try to import from {}".format(filename))
        duples={}
        with open( filename, 'r') as f:
            for ln in f:
                dat = ln.split('|')
                id=int(dat[0][1:], 16)
                (f,i,o) = dat[1:4]
                dep_name = dat[6].replace('Отдел продаж ','').replace('HoReCa ', '')
                branch_name, direction_name = dep_name.split(' ',1)
                if 'центральн' in direction_name:
                    direction_name='Центральный'
                    branch_name = dep_name.replace(' центральный','')
                # if direction_name.starts_with('Алко'):
                #     direction_name = 'Алкоголь'
                if not dep_name in duples:
                    print('\t{} - {}'.format(branch_name, direction_name))
                    duples[dep_name]= True

                try:
                    group = Group.objects.get(branch__name=branch_name,direction__name=direction_name)
                except ObjectDoesNotExist:
                    group = Group.objects.create(
                        branch=self.create_or_get(Branch, branch_name),
                        direction=self.create_or_get(Direction, direction_name),
                        name=' '.join((branch_name, direction_name, ', общая'))
                    )

                try:
                    user = MobileUser.objects.get(first_name=f, last_name=i, middle_name=o)
                    user.department = group
                    user.active = True
                    user.save()
                except ObjectDoesNotExist:
                    user = MobileUser.objects.create(
                        first_name=f,
                        last_name=i,
                        middle_name=o,
                        department=group,
                        active=True)
