import json
from optparse import make_option
from django.core.management.base import BaseCommand
from geoumk.models import *
from geoumk.utils import create_date_filter


class SearchStopPoints:
    options = dict(
        max_speed=350,
        max_acceleration=13.6  # g=9.8 m/sec/sec = 2.72 km/h/sec, 5g = 13.6km/h/sec
    )
    def __init__(self,**kwargs):
        self.options.update(kwargs)
        self.geo_json = {"type": "FeatureCollection", "features": []}
        self.reset()

    def reset(self):
        self.last_speed = None
        self.prev_point = None
        self.stops_list = []
        self.viewed_points = []
        self.last_acceleration = None
        self.avg_acceleration = None
        self.accelerations = []

    def _geo_json_add_point(self, pnt):
        self.geo_json['features'].append({
          "type": "Feature",
          "properties": {'speed':pnt.speed or '', 'ts':pnt.when_gathered.strftime('%H:%M:%S'), 'id': int(pnt.id) },
          "geometry": {
            "type": "Point",
            "coordinates": [
              pnt.lng,
              pnt.lat
            ]
          }
        })


    def process_point(self, pnt):
        t_diff = pnt.timestampsDiff(self.prev_point)
        if t_diff==0:
            # just igtore point with same time
            return
        if t_diff < 0:
            print('!!! wrong point order: {}-({}) before {}-({})'.format(self.prev_point.id, self.prev_point.when_gathered.strftime('%H:%M:%S'), pnt.id, pnt.when_gathered.strftime('%H:%M:%S')))

        pnt.speed=None
        speed = pnt.calcSpeed(self.prev_point)
        if speed<0 or speed>self.options['max_speed']:
            # something wrong with speed
            print('!!! wrong speed {:5.2f}: {} - {}'.format(speed, pnt, self.prev_point))
            # self.prev_point = pnt
            # return

        if self.last_speed:
            self.last_acceleration = ( speed-self.last_speed ) / t_diff # km/h per seconds
            self.accelerations.append( self.last_acceleration )
            if abs(self.last_acceleration) > self.options['max_acceleration']:
                print('!!! to fast speed-up: {:5.2f}km/h to {:5.2f}km/h by {:5.2f} sec'.format(self.last_speed, speed, t_diff))
                return
        self._geo_json_add_point(pnt)

        self.last_speed = speed

        self.prev_point = pnt

    def __call__(self, device_id, *args, **kwargs):
        self.kwargs = kwargs
        filter = ('for_date' in kwargs or 'time_from' in kwargs) and create_date_filter('when_gathered', **kwargs) or {}
        filter['device_id'] = device_id
        self.point_set = Point.objects.filter(**filter).order_by('when_gathered', 'id')
        if self.point_set.count() <=0:
            print('nothing to do')
            return
        print('((( count={}'.format(self.point_set.count()))
        for point in self.point_set:
            if self.prev_point:
                self.process_point( point )
            else:
                self.prev_point = point
            self.viewed_points.append( point )

        print("GEO-JSON:\n\n\n{}".format(json.dumps(self.geo_json)))

class Command( BaseCommand ):
    option_list = BaseCommand.option_list + (
        make_option('--force', '-f', dest='force', default=False, action="store_true",
            help='Force'),
        make_option('--for-date', '-D', dest='for_date',
            help='for_date'),
        make_option('--dev-id', '-d', dest='device_id', type="int",
            help='device id'),
        make_option('--from', '-F', dest='time_from', default=False,
            help='time from'),
        make_option('--to', '-T', dest='time_to', default=False,
            help='time to'),
        make_option('--show-points', '-p', dest='debugPoints', action="store_true",
            help='show-points information'),
    )
    help = 'Help text goes here'

    def handle(self, **options):
        opt = dict([ (nm,options[nm]) for nm in ('force','for_date','device_id','debugPoints') if nm in options and options[nm] ])
        print("Start recalc stops {}".format(opt))

        searcher = SearchStopPoints(**opt)
        searcher(**opt)

# ((45.180584858570136,39.25895690917969)(44.74868389996833,38.62037658691406))
