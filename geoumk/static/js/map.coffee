"use strict"

# angular = window.angular
app = angular.module "umkgeo", [ 'leaflet-directive', 'ui.layout' ]

url_devices = '/geoumk/json/devices'
url_points = '/geoumk/event/getpoint/'
url_path = '/geoumk/event/getline/'

map_devices =
  paths: {}
  markers:{}
  center:
    lat: 45.038960
    lng: 39.080040
    zoom: 10
  legend:
    position:'bottomleft'
    colors:[]
    labels:[]

window.map_devices = map_devices

app.controller "SelectDevicesForm", [ '$scope', ($scope)->
  0
]
class DevicesList
  activeDevices= {}
  currentDevice=null
  constructor: (@$scope, @$http, @$window)->
    @$scope.me = this
    @reloadDeviceList()
    #$interval(@reload,1000)


  reloadDeviceList:() ->
    responsePromise = @$http.get(url_devices)
    $scope = @$scope
    @activeDevices = {}
    responsePromise.success (data, status, headers, config)->
      console.log 'Loaded departments:'+ data.list.length
      $scope.list=data.list

  toggleAllDevices: (department)->
    department.allDevicesActive = !department.allDevicesActive
    for dev in department.devices
      dev.active = department.allDevicesActive
      dev.department = department
    if department.allDevicesActive
      @loadPoints()
    else
      @removeDevicesFromMap( department.devices )

  selectDevice: (device)->
    if @currentDevice
      @removeDeviceStops()
      @currentDevice.current = false
    @currentDevice = device
    device.current = true
    @loadPath( device )

  toggleDevice: (device)->
    return false
    device.active=!device.active
    device.department.allDevicesActive = false if !device.active && device.department
    if device.active
      @loadPoints()
    else
      @removeDevicesFromMap([device])

  removeDevicesFromMap: (devices_to_remove)->
    for dev in devices_to_remove
      delete map_devices.markers['dev_'+dev.id]
    @recalcBounds()

  getActiveDevices: ()->
    @activeDevices = {}
    for dep in @$scope.list
      for dev in dep.devices when dev.active
        @activeDevices[dev.id] = dev
    return @activeDevices

  removeDeviceStops: ()->
     for stop_id in @currentDevice.stops_ids
        delete map_devices.markers['stop_'+stop_id]

  loadPoints: ()->
    activeDevices = @getActiveDevices()
    if not activeDevices?
      return
    loader = @$http.get(url_points,{id:(dev_id for dev_id of activeDevices).join(','),ts:new Date()})
    self=@

    loader.success (data)->
      #currentPosition.clearLayers();
      if data.bounds.northEast.lat == data.bounds.southWest.lat and data.bounds.northEast.lng == data.bounds.southWest.lng
        map_devices.center.lat = data.bounds.northEast.lat
        map_devices.center.lng = data.bounds.northEast.lng

      map_devices.legend.colors = []
      map_devices.legend.labels = []
      map_devices.bounds=data.bounds
      for point in data.points
        dev = activeDevices[point.device_id]
        dev.point = point
        dev.color = dev.color || self.getRandomColor()
        dev.imei = dev.imei || dev.phone || '-{ no phone }-'
        dev.user = dev.user || ' '
        point = angular.extend point,
          color: dev.color
          label:
            message:dev.user+':'+dev.imei+'<p>'+point.speed+' км/ч</p>'
            options:
              noHide: true
          title:dev.user+':'+dev.imei+'<p>'+point.speed+' км/ч</p>'
#          icon:
#            type:'div',
#            className:'currentPosition',
#            iconSize:[20,20],
#            iconAnchor:[10,10]


        map_devices.markers['dev_'+point.device_id] = point

        #for dev_id, dev of activeDevices
        map_devices.legend.colors.push( dev.color )
        map_devices.legend.labels.push( dev.user )

  recalcBounds: ()->
    bounds =
      northEast:{lat:-999.0,lng:-999.0}
      southWest:{lat:999.0,lng:999.0}

    map_devices.bounds = bounds
    for point in map_devices.markers
        if bounds.southWest.lat > parseFloat(pnt.lat)
           bounds.southWest.lat = parseFloat(pnt.lat)
        if bounds.southWest.lng > parseFloat(pnt.lng)
           bounds.southWest.lng = parseFloat(pnt.lng)
        if bounds.northEast.lat < parseFloat(pnt.lat)
           bounds.northEast.lat = parseFloat(pnt.lat)
        if bounds.northEast.lng < parseFloat(pnt.lng)
           bounds.northEast.lng = parseFloat(pnt.lng)
    if bounds.northEast.lat == bounds.southWest.lat and bounds.northEast.lng == bounds.southWest.lng
      map_devices.center.lat = bounds.northEast.lat
      map_devices.center.lng = bounds.northEast.lng
      map_devices.bounds={}
    bounds

  getRandomColor:()->
    c='#'
    for i in [1..3]
      r = parseInt(Math.random() * 100,10) + 150
      c +=r.toString(16)
    return c

  loadPath: (dev)->
    loader = @$http.get(url_path,{id:dev.id,ts:new Date()})
    self=@

    loader.success (data)->
      dev.path_data = data
      map_devices.paths = data.path
      dev.stops_ids = []
      for stop_id, stop_info of data.stops
        dev.stops_ids.push stop_id
        map_devices.markers['stop_'+stop_id] = stop_info


      #overlays


  getHeight:()->
    otherHeight = 120;
    @$window.innerHeight-otherHeight

app.controller "DevicesList", [ '$scope','$http', '$window','$interval', DevicesList]
#app.controller "DevicesList", [ '$scope','$http', ($scope, $http)->
#  responsePromise = $http.get(url_devices)
#  responsePromise.success (data, status, headers, config)->
#    $scope.deparataments=data.list
#]

window.app = app