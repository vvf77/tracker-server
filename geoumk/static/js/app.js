/**
 * Created by vvf on 18.09.14.
 */
/***
 * TODO: translate it to coffee
 */

/* *** load data *** */
(function(){
    var url_devices = '/geoumk/json/devices';
    var $list_deps = $('.list-deps');
    var render = function( json ){
        var $lists = $.map( json.list, function(dep_data){
            var $li = $('<li class="dep"><input type="checkbox" class="select_group"><a href="#"></a><ul style="display:none"/></li>');
            $li
                .attr('id','dep'+dep_data.id)
                .find('a')
                .html( dep_data.name);
            $li.find('ul').append($.map( dep_data.devices, function(device){
                return $('<li guid="'+device.id+'"><a href="" class="check">'+device.imei+'</a></li>');
            }));
            return $li;
        });
        $list_deps.html($lists);
    }
    $.getJSON(url_devices , render);
})();

var url_gps;
var map;
function loadLineExwl(){

        var list_id = "";
        $( ".chek" ).each(function (i) {
            if ( $(this).attr("checke") == "checked" ) {

                var ths = this;

                if(list_id > "")
                    list_id = list_id + ",";

                list_id = list_id + $(ths).parent().attr('guid');
            }
        });
    if(list_id>"")
    location.href='/geoumk/event/linegetexcel/?id='+list_id+'&date1='+$("#date1").val();
    else
    alert("Не выбранны участники!");

}
function decode(encoded, precision) {
    precision = 6;
	precision = Math.pow(10, -precision);
	var len = encoded.length, index=0, lat=0, lng = 0, array = [];
	while (index < len) {
		var b, shift = 0, result = 0;
		do {
			b = encoded.charCodeAt(index++) - 63;
			result |= (b & 0x1f) << shift;
			shift += 5;
		} while (b >= 0x20);
		var dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
		lat += dlat;
		shift = 0;
		result = 0;
		do {
			b = encoded.charCodeAt(index++) - 63;
			result |= (b & 0x1f) << shift;
			shift += 5;
		} while (b >= 0x20);
		var dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
		lng += dlng;
		//array.push( {lat: lat * precision, lng: lng * precision} );
		array.push( [lat * precision, lng * precision] );
	}
	return array;
}

$(document).ready(function() {

    var $list_deps = $('.list-deps');
    /*------------------------------------------------------------------------------------------------------------------*/
    var currentPosition = new L.LayerGroup();

    var d=new Date();
    var day=d.getDate()+1;
    var month=d.getMonth() + 1;
    var year=d.getFullYear();
    if(month<10)
    month="0"+month
    if(day<10)
    day="0"+day
    $('#date2').val(year+"-"+month+"-"+day+"");

    var d=new Date();
    var day=d.getDate();
    var month=d.getMonth() + 1;
    var year=d.getFullYear();
    if(month<10)
    month="0"+month
    if(day<10)
    day="0"+day
    $('#date1').val(year+"-"+month+"-"+day+"");
    /*------------------------------------------------------------------------------------------------------------------*/


    /*------------------------------------------------------------------------------------------------------------------*/
    /*  Позиционирование по таймеру                                                                                     */
    /*------------------------------------------------------------------------------------------------------------------*/

    var myVar=setInterval(function(){myTimer()}, 5000);
    function myTimer()
    {
        //tek();
    }

    /*------------------------------------------------------------------------------------------------------------------*/


    url_gps = "/geoumk/event/getline/" ;


    /*------------------------------------------------------------------------------------------------------------------*/
    /*  Позиционирование элементов на странице                                                                          */
    /*------------------------------------------------------------------------------------------------------------------*/

    var map_resize = function(){
        doc_w = $("body").width();
        doc_h = $(window).height();

        $("#map").css({'width': doc_w-280, 'height': $(".map-content").height()});
        $list_deps.css('height',$('.control-panel').height()-255);
    }
    map_resize();
    $(window).on('resize', map_resize);

//    $("#leftm").css({'height': doc_h+'px'});

    /*------------------------------------------------------------------------------------------------------------------*/


    /*------------------------------------------------------------------------------------------------------------------*/
    /*  Инициализация карты                                                                                             */
    /*------------------------------------------------------------------------------------------------------------------*/

	map = L.map('map').setView([45.03896, 39.08004], 13);

	L.tileLayer('http://10.10.55.48/osm_tiles/{z}/{x}/{y}.png', {
			maxZoom: 18,
			attribution: '  ЮМК  '
		}).addTo(map);

    currentPosition.addTo(map);

    $("#loadinf").hide(); // Скрыть информацию о загрузке

    /*------------------------------------------------------------------------------------------------------------------*/

    /*------------------------------------------------------------------------------------------------------------------*/
    /*                                                                                                                  */
    /*------------------------------------------------------------------------------------------------------------------*/


    function marsh()
    {
        $( ".chek" ).each(function (i) {
        if ( $(this).attr("checke") == "checked" ) {
        var ths = this;

        var stringRequestRoute = "";

        currentPosition.clearLayers();

        $.get(url_gps+"&re="+Math.random()+"&date2="+$("#date2").val()+"&date1="+$("#date1").val()+"&id=" + $(ths).parent().attr('guid'),
                    function(data)
                    {
                        if(data.length > 0)
                        {

                            var myIcon = L.icon({
                                        iconUrl: '/static/gps/images/in.png',
                                        iconSize: [20, 25],
                                        iconAnchor: [20, 25]
                                    });

                            L.marker([data[0].lat, data[0].lon], {icon: myIcon, title: "Начало", alt: "Начало"}).addTo(currentPosition).bindPopup("Начало");

                            var myIcon = L.icon({
                                        iconUrl: '/static/gps/images/ik.png',
                                        iconSize: [20, 25],
                                        iconAnchor: [20, 25]
                                    });

                            L.marker([data[data.length-1].lat, data[data.length-1].lon], {icon: myIcon, title: "Конец", alt: "Конец"}).addTo(currentPosition).bindPopup("Конец");
var sec = 0;
                            for(var ind = 0; ind < data.length; ind++)
                            {
                               // if(data[ind].ps == "1")
                              //  {
                                    var myIcon = L.icon({
                                        iconUrl: '/static/gps/images/ms.png',
                                        iconSize: [8, 8],
                                        iconAnchor: [8, 8]
                                    });

                                    L.marker([data[ind].lat, data[ind].lon], {icon: myIcon, title: "Промежуточная точка", alt: "Промежуточная точка"}).addTo(currentPosition).bindPopup(data[ind].change);
                              //  }

                                if(data[ind].ps == "1")
                                {
                                    sec=sec+1;
                                    var myIcon = L.icon({
                                        iconUrl: '/static/gps/images/num/'+sec+'.png',
                                        iconSize: [20, 25],
                                        iconAnchor: [20, 25]
                                    });

                                    L.marker([data[ind].lat, data[ind].lon], {icon: myIcon, title: "Остановка", alt: "Остановка"}).addTo(currentPosition).bindPopup(data[ind].change);
                                }
                            }

 var array = new Array( '#3366FF', '#000', '#8B0000', '#B8860B', '#Turquoise', '#Green', '#Magenta');
                            var collsors = array[Math.round(Math.random() * 5)];

                            for(var ind = 0; ind < data.length; ind++)
                            {
                                stringRequestRoute = "?loc=" + data[ind].lat + "," + data[ind].lon + "&loc=" + data[ind+1].lat+ "," + data[ind+1].lon;

                                $.getJSON( "http://10.10.55.48:5000/viaroute" + stringRequestRoute, function( data ) {



                                  var polyline = L.polyline( decode( data["route_geometry"], 6 ) , {color: '#3366FF', weight: '7', opacity: '0.8'}).addTo(currentPosition);

                                });
                            }
                        }
                     });
            }
            });
     }


    /*------------------------------------------------------------------------------------------------------------------*/

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}


    /*------------------------------------------------------------------------------------------------------------------*/

    $(".chek").click(function(){


        if ( $(".marshchek").attr("check") == "checked" ) {
          //  marsh();
        }
        else
        {
           // myCollection_marsh.removeAll();
           // myMap.geoObjects.remove(myCollection_marsh);
            $(".info_treck").html("");
        }
        return false;
    });

    var fockus = 0;

    $list_deps.on('click','.select_group',function(){

        if($(this).is(':checked'))
         {
            $(this).parent().children("ul").show();
            $(this).parent().find("ul li a").addClass("action");
            $(this).parent().find("ul li a").attr("checke", "checked") ;
         }
         else
         {
            $(this).parent().children("ul").hide();
            $(this).parent().find("ul li a").removeClass("action");
            $(this).parent().find("ul li a").attr("checke", "") ;
         }
         tek();
    });

    $list_deps.on('click',"a",function(){

         if($(this).hasClass("action"))
         {
            $(this).removeClass("action");
            $(this).attr("checke", "") ;
         }
         else
         {
            $(this).addClass("action");
            $(this).attr("checke", "checked") ;
         }

         if($(this).parent().children("ul").css("display") == "none")
         {
            $(this).parent().children("ul").show();
           // $(this).parent().find("ul li a").addClass("action");
           // $(this).parent().find("ul li a").attr("checke", "checked") ;
         }
         else
         {
            $(this).parent().children("ul").hide();
          //  $(this).parent().find("ul li a").removeClass("action");
           // $(this).parent().find("ul li a").attr("checke", "") ;
         }

         if(fockus == 1)
         {
            $(".left ul ul").hide();
                $(".left ul li a").show();
                $(".left ul li a").each(function (index, domEle) {
                    if($(this).hasClass("action"))
                    {
                        $(this).parent().parent().show();
                        $(this).parent().parent().parent().children("li a").addClass("action");
                        $(this).parent().parent().parent().children("li a").attr("checke", "checked") ;
                    }
                });
                fockus = 0;
         }

         marsh();
        return false;
    });

    $("#btn_seach").keyup(function(){
    fockus = 0;
        $(".left ul").show();
        $(".left ul li a").hide();
         $(".left ul ul li a").each(function (index, domEle) {
        if ($(this).html().toUpperCase().indexOf($("#btn_seach").val().toUpperCase())>-1) {
            $(this).show();
            }

      });
    });

    $("#btn_seach").focusout(function(){
        if($("#btn_seach").val()=="")
        {
            $(".left ul ul").hide();
                $(".left ul li a").show();
                $(".left ul li a").each(function (index, domEle) {
                    if($(this).hasClass("action"))
                    {
                        $(this).parent().parent().show();
                        $(this).parent().parent().parent().children("li a").addClass("action");
                    }
                });
                fockus = 0;
        }
        else
        {
            $("#btn_seach").val("");
            fockus = 1;
        }
    });

    /*------------------------------------------------------------------------------------------------------------------*/

});
