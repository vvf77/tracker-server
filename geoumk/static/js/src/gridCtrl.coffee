angular.module 'umkGeo'
  .controller "umkGridCtrl", ['$scope', 'umkDevicePathService', ($scope, pathSvc)->
    $scope.$watch 'umk.deviceByUrl', ()->
      return unless $scope.umk.deviceByUrl
      return unless $scope.umk.deviceByUrl.stop_id
      return unless $scope.gridOptions.selectItem
      stop_point = $scope.map.markers['sp_'+$scope.umk.deviceByUrl.stop_id]
      return if stop_point.focus  # already selected
      $scope.gridOptions.selectItem stop_point.grid_index, true

    gridLayoutPlugin = new ngGridLayoutPlugin();
    $scope.gridOptions=
      data: 'map.stops'
      enableColumnResize: yes
      multiSelect: no
      enableRowSelection: true
      enableRowHeaderSelection: false
      plugins: [gridLayoutPlugin]
      afterSelectionChange:(row)->
#        msg = 'row selected ' + row.selected;
#        console.log(msg, row);
        marker = $scope.map.markers['sp_'+row.entity.id]
        unless row.selected
          if marker
            marker.focus = false
          return
        $scope.map.center =
          lat: row.entity.lat
          lng: row.entity.lng
          zoom: $scope.map.center.zoom

        $scope.map.center.zoom = $scope.umk.config.zoomLevel + $scope.umk.config.zoomLevelMin if $scope.umk.config.zoomOnSelect
        $scope.map.decors.arrow.coordinates = []

        if marker?
          marker.focus = true
          if marker.grid_index >0
            prev = $scope.map.stops[marker.grid_index-1]
            $scope.map.decors.arrow.coordinates.push([prev.lat, prev.lng])
          $scope.map.decors.arrow.coordinates.push([marker.lat, marker.lng])
          if marker.grid_index < $scope.map.stops.length-1
            next = $scope.map.stops[marker.grid_index+1]
            $scope.map.decors.arrow.coordinates.push([next.lat, next.lng])

      columnDefs: [
        { field:'id' ,displayName:'id', width:'50px', cellClass:'small' }
        { field:'time_stop', displayName:'Время остановки', width:'100px' }
        { field:'how_long', displayName:'Время стоянки', width:'100px' }
        { field:'address', displayName:'Адрес', width:'*', cellClass:'small' }
        { field:'lat', displayName:'Latt', width:'70px', cellClass:'small' }
        { field:'lng', displayName:'Long', width:'70px', cellClass:'small' }
#        { field: null, displayName:'>>>', width:'30px', cellClass:'cb', cellTemplate:'<div><input type="checkbox" ng-model="showarrow"></div>' }
      ]
#  map.markers[].focus = yes
  ]