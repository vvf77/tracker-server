
angular.module 'umkGeo'
  .controller "umkMapCtrl", ['$scope','leafletData', ($scope,leafletData)->
    angular.extend $scope,
      defaults:
        scrollWheelZoom: true
      layers:
        baselayers:
#            yandex:
#              name: 'Yandex'
#              type: 'yandex'
#              layerOptions:
#                layerType: 'map'
            osm:
              name: 'OpenStreetMap'
              type: "xyz"
              url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            osm_local:
              name: 'OSM local'
              type: "xyz"
              url: "http://10.10.55.48/osm_tiles/{z}/{x}/{y}.png"
            stamen_bw:
              name: 'stamen bw'
              type: "xyz"
              url: "http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.png"
            stamen_wc:
              name: 'stamen watercolor'
              type: "xyz"
              url: "http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png"
            mapquest:
              name: 'mapquest'
              type: "xyz"
              url: "http://otile2.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.png"
            googleTerrain:
                name: 'Google Terrain'
                layerType: 'TERRAIN'
                type: 'google'
            googleHybrid:
              name: 'Google Hybrid'
              layerType: 'HYBRID'
              type: 'google'
            googleRoadmap:
              name: 'Google Streets'
              layerType: 'ROADMAP'
              type: 'google'


    unless $scope.map.center
      $scope.map.center =
          lat: 45.038960
          lng: 39.080040
          zoom: 10

  #  leafletData.getMap().then (map)->
  #    map_devices.ll_map = map

  #  TODO: do it though ngRoute
  #  if window.location.hash
  #    hashInfo = window.location.hash.substr(1).split('/')
  #    hashInfo.shift()
  #    $scope.center.lat = parseFloat(hashInfo[0]) || 45.038960
  #    $scope.center.lng = parseFloat(hashInfo[1]) || 39.080040
  #    $scope.center.zoom = parseInt(hashInfo[2],10) || 10

  ]
