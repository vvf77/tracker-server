// Generated by CoffeeScript 1.8.0
(function() {
  var app;

  app = angular.module("umkGeo", ['ngRoute', 'ngGrid', 'ui.layout', 'leaflet-directive', 'ui.bootstrap'], [
    '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
      $routeProvider.when('/Stop/:stopId', {
        controller: 'umkMainCtl'
      });
      $routeProvider.when('/Stop/:stopId/:forDate', {
        controller: 'umkMainCtl'
      });
      $routeProvider.when('/Device/:devId/', {
        controller: 'umkMainCtl'
      });
      $routeProvider.when('/Device/:devId/:forDate', {
        controller: 'umkMainCtl'
      });
      window.$routeProvider = $routeProvider;
      window.$locationProvider = $locationProvider;
      return false;
    }
  ]).controller('umkCalendarPopup', function($scope) {
    $scope.opened = false;
    return $scope.toggle = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      return $scope.opened = !$scope.opened;
    };
  }).controller('umkAsideResizeCtrl', function($scope) {
    var dev_list_resizer;
    dev_list_resizer = function() {
      $scope.dev_list_height = 100;
      $scope.dev_list_height = $('aside').height();
      $('.form').each(function(i, a) {
        var $a;
        $a = $(a);
        return $scope.dev_list_height -= $a.height();
      });
      return $scope.dev_list_height -= 38;
    };
    $(window).resize(dev_list_resizer);
    dev_list_resizer();
    window.aside_scope = $scope;
    return $scope.$on('layout-resize', function() {
      return $(window).resize();
    });
  }).filter('highlight', [
    '$sce', function($sce) {
      return function(b, a, tag) {
        var e, i, x;
        if (tag == null) {
          tag = 'b';
        }
        if (!a) {
          return $sce.trustAsHtml(b);
        }
        i = b.indexOf(a);
        if (i < 0) {
          return $sce.trustAsHtml(b);
        }
        try {
          x = $sce.trustAsHtml(b.substr(0, i) + ("\<" + tag + "\>") + a + ("\</" + tag + "\>") + b.slice(i + a.length));
        } catch (_error) {
          e = _error;
          console.log(e);
        }
        return x;
      };
    }
  ]);

  window.getApp = function() {
    return app;
  };

}).call(this);

//# sourceMappingURL=app.js.map
