reverseGeoUrl = 'http://10.10.55.48/nominatim/reverse.php?format=json&'
#reverseGeoUrl = 'http://nominatim.openstreetmap.org/reverse?format=json&'
# setups of arrowed Line
arrowedLinePatterns = [
#  {
#    offset: 0
#    repeat: 25
#    isOffsetInPixels: true
#    symbol: L.Symbol.dash(
#      pixelSize: 15
#      pathOptions:
#        color: "#550000"
#        weight: 2
#    )
#  }
  {
    offset: 0
    repeat: 50
    symbol: L.Symbol.arrowHead(
      pixelSize: 10
      pathOptions:
        weight: 1
        color: "#900"
    )
  }
]

class CommonHttpService
  @$inject = ['$q','$http', '$filter']

  constructor: (@$q, @$http,@$filter)->

    @deferred = $q.defer()
    @dataPromise = @deferred.promise

  abort: ()->
    return unless @processing
    return unless @aborter
    @aborter.resolve()
    if @deferred
      @deferred.reject()

  _doHttp:( params )->
    params.fordate = @$filter('date')(params.fordate, 'yyyy-MM-dd') if params.fordate instanceof Date
    @deferred = @$q.defer()
    if @processing
      @aborter.resolve()
    @processing = true
    @aborter = @$q.defer()
    # load and draw stop markers and path
    @httpPromise = @$http.get @url,
      params: params
      timeout: @aborter.promise
    self = @
    @httpPromise.success (data)->
      if self.deferred
        self.deferred.resolve(data)
    .then (a,b,c)->
      if self.deferred && a.status != 200
        self.deferred.reject()
      @processing = false
    @dataPromise = @deferred.promise

class CurrentPositions
  url = '/geoumk/api/getpoint/'
  @$inject = ['$q','$http', '$interval']
  reloadInterval = 1000
  prefix = 'wd_'  # Watching Devices

  constructor: (@$q, @$http,@$interval)->
    @lastTimestamp = null
    @doWatching = no
    @map=null
    @deferred = @$q.defer()
    @devs = {}
    self = @
    @processing = false
    @timer = @$interval ()->
      self.reload()
    , reloadInterval
    window.pntSvc = @

  addWatchingDevice: (dev, color)->
    @lastTimestamp = null
    @devs[dev.id] = color
    @

  removeWatchingDevice: (dev)->
    dev_id = dev.id
    delete @map.markers[prefix + dev_id]
    delete @devs[dev.id]
    if @processing
      @aborter.resolve()
    @processing = false
    @lastTimestamp = null
    @

  setMap: (@map)->
    @

  reload: ()->
    self = @
    devList = Object.keys(@devs)
    if @map && @doWatching && @devs && devList.length>0
      if @processing
        @aborter.resolve()
      @processing = true
      @aborter = @$q.defer()
      @httpPromise = @$http.get url,
        params:
          ts:@lastTimestamp or ''
          id: devList.join ','
          bounds:
            lat1: @map.bounds.northEast.lat
            lat2: @map.bounds.southWest.lat
            lng1: @map.bounds.northEast.lng
            lng2: @map.bounds.southWest.lng
        timeout:@aborter.promise

      @httpPromise.then (info)->
        @processing = false
        if info.status == 200
          self.renderToMap( info.data )
          self.deferred.resolve info.data

      @dataPromise = @deferred.promise

  renderToMap: (data)->
    return if data.error?
    for dev in data.devices
      color = @devs[ dev.id ] or '#FFFFDD'
      # todo: remove markers which out of bounds
      @map.markers[prefix + dev.id] =
        lat:dev.lat
        lng:dev.lng
        dev_id: dev.id
        icon:  # TODO: make exclusive mark for devices
          iconSize:[160,55]
          iconAnchor: [83,65]
          popupAnchor:[-5,-63]
          color: color
          html: "<div class='map-device-marker'>#{dev.title}<br/><a href='#/Device/#{dev.id}'>#{dev.id}</a> @ #{dev.time} </div><div class='leaflet-popup-tip-container' ><div class='leaflet-popup-tip'></div></div>"
          type: 'div'
        onClick: ()->
          getAddressFor(dev)

    @lastTimestamp = data.last_timestamp;




class DevicePath
  url = '/geoumk/api/getline/'
  @$inject = ['$q','$http', '$filter']
  deferred = null
  constructor: (@$q, @$http,@$filter)->
    @processing = false
    @

  showPathByStopId:( stop_id, map, forDate=null )->
    return if ! stop_id?
    @_doHttp map,
      stop_id:stop_id,
      fordate:forDate

  showPathOf:( dev, map, forDate=null )->
    return if ! dev?
    @_doHttp map,
      id:dev.id,
      fordate:forDate

  _doHttp:( map, params )->
    params.fordate = @$filter('date')(params.fordate, 'yyyy-MM-dd') if params.fordate instanceof Date
    @deferred = @$q.defer()
    if @processing
      @aborter.resolve()
    @processing = true
    @aborter = @$q.defer()
    # load and draw stop markers and path
    @httpPromise = @$http.get url,
      params: params
      timeout: @aborter.promise
    self = @
    @httpPromise.success (data)->
      self.drawPath data, map
    .then ()->
      @processing = false
    @dataPromise = @deferred.promise


  drawPath: (data, map)->
    # create data for leaflet markers and decors (decors as path - with arrows)
    stopPointsCoords = []
    map.markers = {}
    is_bounds_changed = no
    bounds =
      northEast:{lat:-999.0,lng:-999.0}
      southWest:{lat:999.0,lng:999.0}
    i=0
    for sp in data.stops
      sp.group = 'stoppoint'
      sp.grid_index = i++
      if sp.first_position
        sp.icon=
          iconSize:[20,25]
          iconAnchor: [12,18]
          iconUrl: '/static/gps/images/in.png'
      else if sp.last_position
        sp.icon=
          iconSize:[20,25]
          iconAnchor: [11,25]
          iconUrl: '/static/gps/images/ik.png'
      else
        sp.icon=
          iconSize:[55,68]
          iconAnchor: [28,65]
          popupAnchor:[-5,-63]
          html: "<div class='map-stop-marker'>#{sp.time_stop}<br/>#{sp.how_long}<br/><a href='#/Stop/#{sp.id}'>#{sp.id}</a></div><div class='leaflet-popup-tip-container' ><div class='leaflet-popup-tip'></div></div>"
          type: 'div',

      map.markers['sp_'+sp.id]= sp
      stopPointsCoords.push [sp.lat,sp.lng]
      if bounds.southWest.lat > sp.lat
         bounds.southWest.lat = sp.lat
      if bounds.southWest.lng > sp.lng
         bounds.southWest.lng = sp.lng
      if bounds.northEast.lat < sp.lat
         bounds.northEast.lat = sp.lat
      if bounds.northEast.lng < sp.lng
         bounds.northEast.lng = sp.lng
      is_bounds_changed = yes

      if sp.address
        sp.message = sp.address
      else
        @getAddressFor sp
    if is_bounds_changed and bounds.northEast.lat == bounds.southWest.lat and bounds.northEast.lng == bounds.southWest.lng
      map.center.lat = bounds.northEast.lat
      map.center.lng = bounds.northEast.lng
      bounds={}
    else
      if is_bounds_changed
        map.bounds = bounds
        map.center = {}

    map.stops = data.stops
    map.paths = data.path
    color_step = 255//(Object.keys(map.paths).length-1)
    color_part = 0
    for id, path_info of map.paths
      path_info.weight= 3
      path_info.weight= 4 if id=='all'
      path_info.color = '#00FF'+('0'+color_part.toString(16)).substr(-2) unless path_info.color
      color_part += color_step
#      console.log path_info.latlngs.length
    prev_decor_id = [x for x of map.decors][0]
    map.decors =
      arrow:
        patterns: arrowedLinePatterns


#    map.decors['arrow_'+ data.device_id] =
#      coordinates: stopPointsCoords
#      patterns: arrowedLinePatterns
#    window.setTimeout ()->
#      delete map.decors[prev_decor_id]
#    , 300
    if @deferred
      @deferred.resolve(data)
      @deferred = null

  setLinePatterns: (linePatterns)->
    arrowedLinePatterns = linePatterns

  getAddressFor: (pnt)->
    defer = @$q.defer()
    @$http.get reverseGeoUrl,
      params:
        zoom: 18
        addressdetail: 1
        lon: pnt.lng
        lat: pnt.lat
    .success (address, success) ->
        pnt.address = address.display_name or ''
        pnt.message = address.display_name or ''
        defer.resolve(address.display_name or '', address)
    defer.promise

class Report extends CommonHttpService
  url='/geoumk/report/common.json'
  constructor: (@$q, @$http,@$filter)->
    @url = url
    super

  loadReport: @::_doHttp

class ReportOne extends CommonHttpService
  url='/geoumk/report/detail/'
  constructor: (@$q, @$http,@$filter)->
    @url = url
    super

  loadReport: (dev_id, params)->
    @url = url + dev_id + '.json'
    @_doHttp(params)

angular.module 'umkGeo'
  .service 'umkCurrentPositionsService', CurrentPositions
  .service 'umkDevicePathService', DevicePath
  .service 'umkReportService', Report
  .service 'umkReportOneService', ReportOne
