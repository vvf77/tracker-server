class umkDevicesCtrl
  @$inject = [ '$scope', '$window', '$http', '$q','$modal']
  url_departments = '/geoumk/api/departments/'
  url_devs_prefix = '/geoumk/api/devices/'

  constructor: (@$scope, @$window, @$http, @$q, @$modal)->
    @$scope.ctrl = @
    @$scope.me = @
    window.debug_dev = @
    @$scope.lookForDevice = ''
#    TODO: load it from local stored config/state
    @currentDir = null
    @currentBranch = null

    @$scope.list = []
    @loaded_devs = {}

    self = @
    $scope.$watch 'umk.deviceByUrl', ()->
      self.checkRouteSelected()

    $scope.$watch 'lookForDevice', ()->
      self.lookForDevice()

    $scope.$watch 'ctrl.currentDir', ()->
      self.refresh(false)

    $scope.$watch 'ctrl.currentBranch', ()->
      self.refresh(false)

    @load_deps().then ()->
      self.refresh(false)


  hightLight: (b,a, tag='b')->
    i = b.indexOf(a)
    return if i<0
    b[0..i] + "<#{tag}>" + a + "</#{tag}>" + b[i+a.length..]

  lookForDevice: ()->
    for dep in @$scope.list
      dep.fouded = 0
      for dev in dep.devices
        if @$scope.lookForDevice
          dev.lookForFiltred = dev.imei.indexOf( @$scope.lookForDevice ) < 0
          dep.fouded +=1 unless dev.lookForFiltred
        else
          dev.lookForFiltred = false
      dep.active = (dep.fouded > 0)

  load_deps: ()->
    self = @
    @$scope.branches = []
    @$scope.directions = []
    @$http.get url_departments
      .success (data, status, headers, config)->
        self.$scope.branches = data.branches
        self.$scope.directions = data.directions
        self.currentDir = data.directions[0]
        self.currentBranch = data.branches[0]

  refresh: ( force=yes )->
    self = @
    return if not @currentBranch or not @currentDir
    load_suffix = @currentDir.id+'/'+@currentBranch.id+'/'
    if not force and @loaded_devs[load_suffix]
      if @loaded_devs[load_suffix] != 'loading...'
        @$scope.list = @loaded_devs[load_suffix]
        @checkRouteSelected()
      # return if it list was loaded before or is loading
      return

    @$scope.list_loading = true
    @loaded_devs[load_suffix] = 'loading...'
    @$http.get url_devs_prefix + load_suffix
      .success (data, status, headers, config)->
        self.$scope.list = data.list
        self.loaded_devs[load_suffix] = self.$scope.list
        self.$scope.list_loading = false
        self.checkRouteSelected()

    @$scope.list = []

  selectDevice: (device)->
    # when first time other device select - reset deviceByUrl
    @$scope.umk.deviceByUrl = null
    if @$scope.umk.currentDevice
#      @removeDeviceStops()
      @$scope.umk.currentDevice.current = no
    device.current = true
#    @$scope.umk.currentDevice = device
    @$scope.setCurrentDevice(device)


  checkRouteSelected: ()->
    return unless @$scope.list
    return unless @$scope.umk.deviceByUrl
    return unless @$scope.umk.deviceByUrl.id
    return if @$scope.umk.currentDevice and @$scope.umk.currentDevice.id == @$scope.umk.deviceByUrl.id
    $scope = @$scope
    dep = null
    for dep in @$scope.list
      break if dep.id == @$scope.umk.deviceByUrl.department_id
    return unless dep?
    for dev in dep.devices
      break if dev.id == @$scope.umk.deviceByUrl.id
    return unless dev?
    dep.active = yes
    dev.current = yes
    if @$scope.umk.currentDevice
      @$scope.umk.currentDevice.current = no
    @$scope.umk.currentDevice = dev
    # scroll selected device to the middle
    window.setTimeout ()->
      $ul = $ '.department-list'
      $li = $ul.find '.device-list .active'
      scrollPos = $li.offset().top - $ul.offset().top- ($scope.dev_list_height-$li.height()) // 2
      $ul.scrollTop scrollPos
    , 1

  detail_report: ( device )->
    detail_report_wnd = @$modal.open
      scope: @$scope
      size:'lg'
      resolve:
        device:()-> device
      templateUrl: 'report_detail.html'
      #windowTemplateUrl: 'window.html'
      controller:  'umkDetailReportCtrl'
    detail_report_wnd.device = device

class umkDetailReportCtrl
  @$inject = [ '$scope', '$modalInstance', 'device', 'umkReportOneService', '$q','$timeout']
  constructor: (@$scope, @$modalInstance, @device, @reportSvc, @$q, @$timeout)->
    @$scope.ctrl = @
    @$scope.device = @device
    @$scope.report_data_src = 'report_result'
    @$scope.cfg=
      minutes_per_row: 10
      report_recalc_stops:false
      no_address: true
    self = @
    @$scope.$watch 'cfg.minutes_per_row', ()->
      self.report()
    ,true
    @$scope.$watch 'cfg.report_recalc_stops', ()->
      self.report() if self.$scope.cfg.report_recalc_stops
    ,true
    @$scope.$watch 'cfg.no_address', ()->
      self.report() unless self.$scope.cfg.no_address
    @$scope.height = $(window).height()-150
    @report()


  close: ()->
    @$modalInstance.close()

  report: ()->
    self = @
    @$timeout.cancel( @timer ) if @timer
    @timer = @$timeout ()->
      @timer = null
    , 500
    @timer.then ()->
      self.request_report()
      @timer = null

  request_report: ()->
    params=
      fordate: @$scope.umk.get_forDate()
      time_step: @$scope.cfg.minutes_per_row

    params.recalc_stops=1 if @$scope.cfg.report_recalc_stops
    params['no-address']=1 if @$scope.cfg.no_address
    console.log 'refresh report:', params
    unless @$scope.report_result
      @$scope.report_result =
        meta_data:
          table:[]
        data:
          table:[]
    @$scope.report_result.updating = true
    self = @
    @reportSvc.loadReport(@device.id, params).then (d)->
      self.$scope.report_result = d
      self.$scope.cfg.report_recalc_stops = false
      for column in d.meta_data.table
        column.width = '*' unless column.width
        column.cellTemplate = 'cell_url.html' if column.link_field

angular.module 'umkGeo'
  .controller "umkDevicesCtrl", umkDevicesCtrl
  .controller "umkDetailReportCtrl", umkDetailReportCtrl
