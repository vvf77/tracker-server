# Global controller
# contain common scope (root)
# TODO: make it as singleton class

angular.module 'umkGeo'
  .controller "umkMainCtrl", [
    '$scope', 'umkDevicePathService',
    '$route', '$filter', '$rootScope',
    '$timeout','umkCurrentPositionsService','umkReportService','$modal'
  ($scope, pathSvc, $route, $filter, $rootScope,$timeout, posSvc, reportSvc, $modal)->
    $scope.map=
      decors:{}
      markers:{}
      paths:{}
      stops:[]
      tiles:
        options:
          maxZoom: 18
          attribution: 'ЮМК'

    last_centred_dev_id = null;

    $scope.umk=
      currentDevice:null
      forDate: $filter('date')(new Date(), 'yyyy.MM.dd')
      deviceByUrl:{}
      loading:true
      track_devs:{}
      report_devs:{}
      track_mode: 'track'

      config:
        zoomOnSelect:yes
        showArrows:  no
        zoomLevel: 7
        zoomLevelMin:8

      get_forDate: (tpl='yyyy-MM-dd')->
        for_date = $scope.umk.forDate
        for_date = $filter('date')(for_date, tpl) if for_date instanceof Date
        return for_date

#    $scope.setTiles = (k)->
#      url = $scope.umk.possibleTiles[k]
#      url = url.url if url.url?
#      $scope.map.tiles.url = url
#      $scope.umk.config.tiles=k
#
#    $scope.setTiles('OSM')

    $scope.posSvc = posSvc
    $scope.reportSvc = reportSvc

    window.umk = $scope.umk
    window.debug_map = $scope.map
    initialLoadingTimer = $timeout ()->
      $scope.umk.loading = false
    ,1500

    ## Part of tracking current position

    posSvc.setMap $scope.map

    posSvc.deferred.promise.then ()->
      if last_centred_dev_id
        $scope.centerToDev last_centred_dev_id

    $scope.$watch 'umk.track_mode', ()->
      posSvc.doWatching = $scope.umk.track_mode == 'points'
#      if $scope.umk.track_mode
#        $scope.map.paths = {}
#        $scope.map.stops = []
#        $scope.map.decors = {}
#      else
      if $scope.umk.track_mode == 'points'
        # remove stoppinte and path from track
        for sp in $scope.map.stops
          console.log 'delete marker', sp, $scope.map.markers['sp'+sp.id]
          delete $scope.map.markers['sp_' + sp.id]
        $scope.map.stops=[]
        $scope.map.paths={}
        $scope.map.decors = {}
      else
        for dev , state of $scope.umk.track_devs
          delete $scope.map.markers[dev.replace('dev','wd_')] if state
        posSvc.lastTimestamp = null
      if $scope.umk.track_mode == 'track' and $scope.umk.currentDevice and $scope.umk.forDate
        for_date = $scope.umk.forDate
        for_date = $filter('date')(for_date, 'yyyy-MM-dd') if for_date instanceof Date
        window.location.hash="/Device/#{$scope.umk.currentDevice.id}/" + for_date+'/'
      $timeout ()->
        $(window).resize()
      , 1

    $scope.centerToDev = (dev_id)->
      last_centred_dev_id = dev_id
      if $scope.umk.track_mode == 'points'
        dev_marker = $scope.map.markers['wd_'+dev_id]
        if dev_marker
          $scope.map.center=
            lat:dev_marker.lat
            lng:dev_marker.lng
            zoom: $scope.umk.config.zoomLevel + $scope.umk.config.zoomLevelMin
      else
        forDate = $scope.umk.forDate
        forDate = $filter('date')(forDate, 'yyyy.MM.dd') if forDate instanceof Date
        window.location.hash = '/Device/'+dev_id+'/'+ forDate + '/'

    $scope.changeTrackDevice = (dev)->
      if $scope.umk.track_devs['dev'+dev.id]
        posSvc.removeWatchingDevice dev
      else
        # TODO: make color
        posSvc.addWatchingDevice dev,'#FFDD55'

    enumerate_devs = (dep, cb, cb2)->
      return unless dep
      if dep.devices
        for dev in dep.devices
          cb(dev)
      else
        for d in dep
          enumerate_devs( d, cb)
          cb2( d )


    $scope.toggleTrackAllInDepartment = (dep, status, $event, where_store = 'track_devs')->
      $event.stopPropagation()
      where_store_obj = $scope.umk[where_store]
      enumerate_devs dep, (dev)->
        if status
          posSvc.addWatchingDevice dev,'#FFDD55'
        else
          posSvc.removeWatchingDevice dev
        where_store_obj['dev'+dev.id] = status
        delete where_store_obj['dev'+dev.id] unless status
      , (dep)->
        dep[where_store] = status

    $scope.getCheckedDevs = (where_store,as_string=true)->
      ret=[]
      for k,v of $scope.umk[where_store]
        ret.push(k.substr(3)) if v
      return ret.join(',') if as_string
      return ret


    $scope.$on '$routeChangeSuccess',()->
#      console.log 'route is =',$route.current,
#      if $route.current
#        console.log '  ... params  is =',$route.current.params
      if $route.current
        if $route.current.params.forDate
          $scope.umk.forDate = $route.current.params.forDate
        if $route.current.params.stopId
          stop_id = $route.current.params.stopId
          window.clearTimeout initialLoadingTimer if initialLoadingTimer
          initialLoadingTimer = null
          # do not reload it
          if $scope.map.markers['sp_'+$route.current.params.stopId]?
            # TODO: select this stop point in grid (and it will show popup)
            # $scope.map.markers['sp_'+$route.current.params.stopId].
            return
          $scope.umk.loading = true
#          $scope.$apply()
          $scope.umk.track_mode = 'track'
          pathSvc.showPathByStopId $route.current.params.stopId, $scope.map, $scope.umk.forDate
            .then (data)->
              $scope.umk.loading = false
              $scope.$apply() unless $rootScope.$$phase
              return unless data.device_id?
              $scope.umk.forDate = data.for_date
              $scope.umk.deviceByUrl =
                id: data.device_id
                department_id: data.department_id
                stop_id: stop_id
                forDate: data.for_date
        if $route.current and $route.current.params.devId
          dev_id = $route.current.params.devId
          window.clearTimeout initialLoadingTimer if initialLoadingTimer
          initialLoadingTimer = null
          $scope.umk.loading = true
          $scope.umk.track_mode = 'track'
#          $scope.$apply()
          pathSvc.showPathOf {id:dev_id}, $scope.map, $scope.umk.forDate
            .then (data)->
              $scope.umk.loading = false
              $scope.$apply() unless $rootScope.$$phase
              return unless data.device_id?
              $scope.umk.forDate = data.for_date
              $scope.umk.deviceByUrl =
                id: data.device_id
                department_id: data.department_id
                forDate: data.for_date

    $scope.$watch 'umk.report_devs', ()->
      return unless $scope.umk.track_mode =='report' and $scope.umk.report_result
      $scope.report()
    ,true


    $scope.$watch 'umk.forDate', ()->
      if $scope.umk.track_mode =='report'
        $scope.report() if $scope.umk.report_result
        return
      return if $scope.umk.deviceByUrl and $scope.umk.deviceByUrl.forDate == $scope.umk.forDate
      if $scope.umk.currentDevice and $scope.umk.currentDevice.id
        for_date = $scope.umk.forDate
        for_date = $filter('date')(for_date, 'yyyy-MM-dd') if for_date instanceof Date
        window.location.hash="/Device/#{$scope.umk.currentDevice.id}/" + for_date
#      pathSvc.showPathOf $scope.umk.currentDevice, $scope.map, $scope.umk.forDate or $filter('date')(new Date(), 'yyyy.MM.dd')

#    $scope.setCurrentDevice = (cd, reload=false)->
#      return if $scope.umk.currentDevice and $scope.umk.currentDevice.id == cd.id and not reload
#      $scope.umk.currentDevice = cd
#      # load and show path
#      pathSvc.showPathOf $scope.umk.currentDevice, $scope.map, $scope.umk.forDate or $filter('date')(new Date(), 'yyyy.MM.dd')

    $scope.config= ()->
      cfg_window = $modal.open
        scope: $scope
        templateUrl: 'config.html'
        controller:  'umkModalCtrl'

#

    $scope.report= ()->
      params=
        devices: $scope.getCheckedDevs('report_devs')
        fordate: $scope.umk.get_forDate()
      params.recalc_stops=1 if $scope.report_recalc_stops
      unless $scope.umk.report_result
        $scope.umk.report_result =
          meta_data:
            table:[]
          data:
            table:[]
      $scope.umk.report_result.updating = true
      reportSvc.loadReport(params).then (d)->
        $scope.umk.report_result = d
        for column in d.meta_data.table
          column.width = '*' unless column.width

  ]
.controller "umkModalCtrl", [ '$scope','$modalInstance', ($scope, $modalInstance)->
    $scope.close = ()->
      $modalInstance.close()
  ]
.controller "umkReportCommonCtrl", [ '$scope', ($scope)->

  gridLayoutPlugin = new ngGridLayoutPlugin()
  $scope.report_data_src='umk.report_result' unless $scope.report_data_src
  $scope.gridOptions =
    plugins:[gridLayoutPlugin ]
    columnDefs:$scope.report_data_src + '.meta_data.table'
    data:$scope.report_data_src+'.data.table'
    enableColumnResize: true
    enableRowSelection: false
    enableRowHeaderSelection: false
    enableSorting: false
]