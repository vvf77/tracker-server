
app = angular.module "umkGeo", ['ngRoute','ngGrid','ui.layout','leaflet-directive','ui.bootstrap'], ['$routeProvider', '$locationProvider',($routeProvider, $locationProvider) ->
    $routeProvider.when '/Stop/:stopId',
      controller: 'umkMainCtl'
    $routeProvider.when '/Stop/:stopId/:forDate',
      controller: 'umkMainCtl'
    $routeProvider.when '/Device/:devId/',
      controller: 'umkMainCtl'
    $routeProvider.when '/Device/:devId/:forDate',
      controller: 'umkMainCtl'
    window.$routeProvider = $routeProvider
    window.$locationProvider = $locationProvider
    no
  ]
  .controller 'umkCalendarPopup', ($scope)->
    $scope.opened= false
    $scope.toggle = ($event)->
      $event.preventDefault()
      $event.stopPropagation()
      $scope.opened = !$scope.opened

  .controller 'umkAsideResizeCtrl', ($scope)->
    dev_list_resizer = ()->
      $scope.dev_list_height = 100
      $scope.dev_list_height = $('aside').height()
      $('.form').each (i,a)->
        $a=$(a)
        $scope.dev_list_height -= $a.height() # + $a.offset().top
      $scope.dev_list_height -= 38
    $(window).resize dev_list_resizer
    dev_list_resizer()
    window.aside_scope=$scope
    $scope.$on 'layout-resize', ()->
#      console.log('layout-resize event')
      $(window).resize()

  .filter 'highlight',['$sce',($sce)->
    (b,a,tag='b')->
      return $sce.trustAsHtml(b) unless a
      i = b.indexOf(a)
      return $sce.trustAsHtml(b) if i<0
      try
        x = $sce.trustAsHtml b.substr(0,i) + "\<#{tag}\>" + a + "\</#{tag}\>" + b[i+a.length..]
      catch e
        console.log e
      x
  ]

window.getApp = ()->
  app
