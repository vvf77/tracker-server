# -*- coding: utf-8 -*-
__author__ = 'vvf'
# DJANGO_SETTINGS_MODULE="gps2.settings.testing"

from .base import *

SOUTH_TESTS_MIGRATE = False
DATABASES={
    'default': {'ENGINE': 'django.db.backends.sqlite3'}
}